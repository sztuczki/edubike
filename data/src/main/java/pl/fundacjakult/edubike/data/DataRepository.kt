package pl.fundacjakult.edubike.data

import io.reactivex.Completable
import io.reactivex.Single
import pl.fundacjakult.edubike.domain.model.LessonModel
import pl.fundacjakult.edubike.domain.model.CompletedLessonModel
import pl.fundacjakult.edubike.domain.model.LessonContentPartModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataRepository @Inject constructor(
        private val dataPersistentSource: DataPersistentSource
) {

    fun getLessons(): Single<List<LessonModel>> {
        return dataPersistentSource.getLessons()
    }

    fun getCompletedLessons(): Single<List<CompletedLessonModel>> {
        return dataPersistentSource.getCompletedLessons()
    }

    fun getContentPartsForLesson(lessonId: String): Single<List<LessonContentPartModel>> {
        return dataPersistentSource.getContentPartsForLesson(lessonId)
    }

    fun saveSolvedLesson(completedLesson: CompletedLessonModel): Completable {
        return dataPersistentSource.saveSolvedLesson(completedLesson)
    }

    interface DataPersistentSource {
        fun getLessons(): Single<List<LessonModel>>
        fun getCompletedLessons(): Single<List<CompletedLessonModel>>
        fun saveSolvedLesson(completedLesson: CompletedLessonModel): Completable

        fun getContentPartsForLesson(lessonId: String): Single<List<LessonContentPartModel>>
    }

    interface DataOnlineSource {
        fun updateContentData()
        fun getDatabaseVersion()
    }
}