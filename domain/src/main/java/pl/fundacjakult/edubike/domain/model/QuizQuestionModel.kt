package pl.fundacjakult.edubike.domain.model

interface QuizQuestionModel {

    val id: Long
    val lesson: String
    val question: String
    val answer: String
    val optionA: String
    val optionB: String
    val optionC: String
    val optionD: String
    val imageUrl: String?
    val filename: String?
    var chosenAnswer: String?
}