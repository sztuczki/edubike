package pl.fundacjakult.edubike.domain.model

interface LessonContentPartModel {

    val id: Long
    val lesson: String
    val data: String
    val type: String
    val filename: String?
    val caption: String?

}