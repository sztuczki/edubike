package pl.fundacjakult.edubike.domain.model

interface LessonModel {
    val lessonId: String
    val name: String
    val category: String
    var isCompleted: Boolean
}