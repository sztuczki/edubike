package pl.fundacjakult.edubike.domain.model

interface CompletedLessonModel {
    var id: String
}