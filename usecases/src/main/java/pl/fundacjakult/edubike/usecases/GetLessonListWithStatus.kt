package pl.fundacjakult.edubike.usecases

import androidx.lifecycle.LifecycleObserver
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import pl.fundacjakult.edubike.data.DataRepository
import pl.fundacjakult.edubike.domain.model.LessonModel
import javax.inject.Inject

class GetLessonListWithStatus @Inject constructor(private val localRepository: DataRepository) : LifecycleObserver {

    operator fun invoke(): Single<List<LessonModel>> {
        val completedLessonIdList = localRepository.getCompletedLessons()
                .map { completedLessonList ->
                    val completedLessonIdList = ArrayList<String>()
                    for (lesson in completedLessonList) {
                        completedLessonIdList.add(lesson.id)
                    }
                    return@map completedLessonIdList
                }
        return localRepository.getLessons().zipWith(completedLessonIdList, BiFunction { lessonList: List<LessonModel>, solvedLessonList: List<String> ->
            for (lesson in lessonList) {
                if (lesson.lessonId in solvedLessonList) lesson.isCompleted = true
            }
            lessonList
        })
    }
}