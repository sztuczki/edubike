package pl.fundacjakult.edubike.usecases

import io.reactivex.Single
import pl.fundacjakult.edubike.data.DataRepository
import pl.fundacjakult.edubike.domain.model.LessonContentPartModel

class GetContentPartsForLesson(private val localRepository: DataRepository) {

    operator fun invoke(lessonId: String): Single<List<LessonContentPartModel>> {
        return localRepository.getContentPartsForLesson(lessonId)
                .map { contentPartFrameworkModelList: List<LessonContentPartModel> ->
                    contentPartFrameworkModelList
                }
    }
}