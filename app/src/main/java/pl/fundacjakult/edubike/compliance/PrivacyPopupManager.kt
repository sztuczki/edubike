package pl.fundacjakult.edubike.compliance

import android.content.Context
import android.preference.PreferenceManager

class PrivacyPopupManager(private val context: Context) {

    fun wasPrivacyPopupDisplayed(): Boolean {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("PrivacyPopupDisplayed", false)
    }

    fun onPrivacyPopupDisplayed() {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("PrivacyPopupDisplayed", true).apply()
    }
}