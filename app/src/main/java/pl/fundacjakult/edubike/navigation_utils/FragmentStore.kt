package pl.fundacjakult.edubike.navigation_utils

import pl.fundacjakult.edubike.domain.model.LessonModel
import pl.fundacjakult.edubike.framework.model.LessonFrameworkModel
import pl.fundacjakult.edubike.features.lesson.LessonFragment
import pl.fundacjakult.edubike.features.lesson_list.LessonListFragment
import pl.fundacjakult.edubike.features.menu.MenuFragment
import pl.fundacjakult.edubike.features.partners.PartnersFragment
import pl.fundacjakult.edubike.features.quiz.QuizFragment
import pl.fundacjakult.edubike.features.video_lesson.VideoLessonListFragment

class FragmentStore {

    fun getMenuFragment(): MenuFragment {
        return MenuFragment()
    }

    fun getLessonListFragment(): LessonListFragment {
        return LessonListFragment()
    }

    fun getVideoLessonListFragment(): VideoLessonListFragment {
        return VideoLessonListFragment()
    }

    fun getLessonFragment(lesson: LessonModel): LessonFragment {
        val fragment = LessonFragment()
        fragment.setLesson(lesson)
        return fragment
    }

    fun getQuizFragment(lessonId: String): QuizFragment {
        val fragment = QuizFragment()
        fragment.setLesson(lessonId)
        return fragment
    }

    fun getBigQuizFragment(): QuizFragment {
        return QuizFragment()
    }

    fun getPartnersFragment(): PartnersFragment {
        return PartnersFragment()
    }
}