package pl.fundacjakult.edubike.navigation_utils

import android.content.Context
import android.content.Intent
import android.net.Uri

class WebBrowserIntentProvider(private val context: Context) {

    fun launchFacebookEventsIntent() {
        val url = "https://pl-pl.facebook.com/pg/fundacjakult/events/?ref=page_internal"
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        context.startActivity(intent)
    }

    fun launchPrivacyPolicyWebsiteIntent() {
        val url = "https://fundacjakult.pl/edubike-privacy-policy/"
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        context.startActivity(intent)
    }
}