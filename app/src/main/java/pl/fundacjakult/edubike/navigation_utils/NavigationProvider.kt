package pl.fundacjakult.edubike.navigation_utils

interface NavigationProvider {

    fun getNavigator(): Navigator
}