package pl.fundacjakult.edubike.navigation_utils

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import pl.fundacjakult.edubike.R
import pl.fundacjakult.edubike.domain.model.LessonModel
import pl.fundacjakult.edubike.framework.model.LessonFrameworkModel
import pl.fundacjakult.edubike.features.lesson_list.LessonListFragment
import javax.inject.Inject

class Navigator @Inject constructor(private val fragmentManager: FragmentManager) {

    private val fragmentStore = FragmentStore()

    fun navigateToMenuFragment() {
        val fragment = fragmentStore.getMenuFragment()
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit()
    }

    fun navigateToLessonListFragment() {
        val fragment = fragmentStore.getLessonListFragment()
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(LessonListFragment::class.simpleName)
                .commit()
    }

    fun navigateToVideoLessonListFragment() {
        val fragment = fragmentStore.getVideoLessonListFragment()
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit()
    }

    fun navigateToLessonFragment(lesson: LessonModel) {
        val fragment = fragmentStore.getLessonFragment(lesson)
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit()
    }

    fun navigateToQuizFragment(lessonId: String) {
        val fragment = fragmentStore.getQuizFragment(lessonId)
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit()
    }

    fun navigateToBigQuizFragment() {
        val fragment = fragmentStore.getBigQuizFragment()
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit()
    }

    fun navigateToPartnersFragment() {
        val fragment = fragmentStore.getPartnersFragment()
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .addToBackStack(null)
                .commit()
    }

    fun navigateBack() {
        if (!isBackStackEmpty()) fragmentManager.popBackStack()
    }

    fun isBackStackEmpty(): Boolean {
        return fragmentManager.backStackEntryCount <= 0
    }

    fun returnToLessonListFragment() {
        fragmentManager.popBackStack(LessonListFragment::class.simpleName, 0)
    }
}