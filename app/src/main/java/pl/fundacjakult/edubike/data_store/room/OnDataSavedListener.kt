package pl.fundacjakult.edubike.data_store.room

interface OnDataSavedListener {

    fun onLessonListSaved()
    fun onContentPartListSaved()
    fun onQuizQuestionListSaved()
}