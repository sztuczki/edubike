package pl.fundacjakult.edubike.data_store.room.room_cleaner

interface OnRoomDataDeletedListener {

    fun onRoomDataDeleted()
}