package pl.fundacjakult.edubike.data_store.room

import io.reactivex.Completable
import io.reactivex.Single
import pl.fundacjakult.edubike.data.DataRepository
import pl.fundacjakult.edubike.domain.model.CompletedLessonModel
import pl.fundacjakult.edubike.domain.model.LessonContentPartModel
import pl.fundacjakult.edubike.domain.model.LessonModel
import pl.fundacjakult.edubike.framework.model.CompletedLessonFrameworkModel
import pl.fundacjakult.edubike.framework.model.LessonContentPartFrameworkModel
import pl.fundacjakult.edubike.framework.model.LessonFrameworkModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RoomDataStore @Inject constructor(private val localDao: RxLocalDao) : DataRepository.DataPersistentSource {

    override fun getLessons(): Single<List<LessonModel>> {
        return localDao.getLessons()
                .map { lessonFrameworkModelList: List<LessonFrameworkModel> ->
                    lessonFrameworkModelList as List<LessonModel>
                }
    }

    override fun getCompletedLessons(): Single<List<CompletedLessonModel>> {
        return localDao.getSolvedLessons()
                .map { completedLessonFrameworkModelList: List<CompletedLessonFrameworkModel> ->
                    completedLessonFrameworkModelList as List<CompletedLessonModel>
                }
    }

    override fun saveSolvedLesson(completedLesson: CompletedLessonModel): Completable {
        val frameworkModel = CompletedLessonFrameworkModel().also { it.id = completedLesson.id }
        return localDao.saveSolvedLesson(frameworkModel)
    }

    override fun getContentPartsForLesson(lessonId: String): Single<List<LessonContentPartModel>> {
        return localDao.getLessonContents(lessonId)
                .map { lessonContents: List<LessonContentPartFrameworkModel> ->
                    lessonContents as List<LessonContentPartModel>
                }
    }
}