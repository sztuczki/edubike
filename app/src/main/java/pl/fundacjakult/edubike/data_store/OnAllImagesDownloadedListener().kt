package pl.fundacjakult.edubike.data_store

interface OnAllImagesDownloadedListener {

    fun onAllImagesDownloaded()
}