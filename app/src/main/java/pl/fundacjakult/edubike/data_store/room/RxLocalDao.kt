package pl.fundacjakult.edubike.data_store.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Single
import pl.fundacjakult.edubike.framework.model.LessonContentPartFrameworkModel
import pl.fundacjakult.edubike.framework.model.LessonFrameworkModel
import pl.fundacjakult.edubike.framework.model.QuizQuestionFrameworkModel
import pl.fundacjakult.edubike.framework.model.CompletedLessonFrameworkModel

@Dao
interface RxLocalDao {

    @Query("SELECT * FROM lesson_list")
    fun getLessons(): Single<List<LessonFrameworkModel>>

    @Query("SELECT * FROM quiz_question_list")
    fun getQuizzes(): List<QuizQuestionFrameworkModel>

    @Query("SELECT * FROM content_part_list where lesson = :lessonId")
    fun getLessonContents(lessonId: String): Single<List<LessonContentPartFrameworkModel>>

    @Query("SELECT * FROM solved_lesson_list")
    fun getSolvedLessons(): Single<List<CompletedLessonFrameworkModel>>

    @Insert
    fun saveSolvedLesson(solvedLesson: CompletedLessonFrameworkModel): Completable
}