package pl.fundacjakult.edubike.data_store.firebase

import com.google.firebase.database.*
import pl.fundacjakult.edubike.features.video_lesson.OnVideoLessonListAvailableListener

class DataStore(private val referenceStore: ReferenceStore) : DataStoreProvider {

    private val extractor = FirebaseDataExtractor()
    private var onDataAvailableListener: OnDataAvailableListener? = null
    private var onVideoLessonListAvailableListener: OnVideoLessonListAvailableListener? = null

    override fun addOnDataAvailableListener(onDataAvailableListener: OnDataAvailableListener) {
        this.onDataAvailableListener = onDataAvailableListener
    }

    override fun attachVersionListener() {
        val reference = referenceStore.getDatabaseVersionReference()
        reference.addValueEventListener(getDatabaseVersionValueEventListener(reference))
    }

    override fun attachLessonListListener() {
        val reference = referenceStore.getLessonListReference()
        reference.addValueEventListener(getLessonListValueEventListener(reference))
    }

    override fun attachContentPartListListener() {
        val reference = referenceStore.getContentPartReference()
        reference.addValueEventListener(getContentPartListValueEventListener(reference))
    }

    override fun attachQuizQuestionListListener() {
        val reference = referenceStore.getQuizQuestionReference()
        reference.addValueEventListener(getQuizQuestionListValueEventListener(reference))
    }

    override fun addOnVideoLessonListAvailableListener(onVideoLessonListAvailableListener: OnVideoLessonListAvailableListener) {
        this.onVideoLessonListAvailableListener = onVideoLessonListAvailableListener
    }

    override fun attachVideoLessonListListener() {
        val reference = referenceStore.getVideoLessonListReference()
        reference.addValueEventListener(getVideoLessonListValueEventListener(reference))
    }

    private fun getDatabaseVersionValueEventListener(reference: DatabaseReference): ValueEventListener {
        return object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val databaseVersion = dataSnapshot.getValue(String::class.java)
                onDataAvailableListener?.onDatabaseVersionAvailable(databaseVersion!!)
                reference.removeEventListener(this)
            }

            override fun onCancelled(databaseError: DatabaseError) {
                reference.removeEventListener(this)
            }
        }
    }

    private fun getLessonListValueEventListener(reference: DatabaseReference): ValueEventListener {
        return object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val lessonList = extractor.extractLessonList(dataSnapshot)
                onDataAvailableListener?.onLessonListAvailable(lessonList)
                reference.removeEventListener(this)
            }

            override fun onCancelled(databaseError: DatabaseError) {
                reference.removeEventListener(this)
            }
        }
    }

    private fun getContentPartListValueEventListener(reference: DatabaseReference): ValueEventListener {
        return object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val contentPartList = extractor.extractContentPartList(dataSnapshot)
                onDataAvailableListener?.onContentPartListAvailable(contentPartList)
                reference.removeEventListener(this)
            }

            override fun onCancelled(databaseError: DatabaseError) {
                reference.removeEventListener(this)
            }
        }
    }

    private fun getQuizQuestionListValueEventListener(reference: DatabaseReference): ValueEventListener {
        return object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val quizQuestionList = extractor.extractQuizQuestionList(dataSnapshot)
                onDataAvailableListener?.onQuizQuestionListAvailable(quizQuestionList)
                reference.removeEventListener(this)
            }

            override fun onCancelled(databaseError: DatabaseError) {
                reference.removeEventListener(this)
            }
        }
    }

    private fun getVideoLessonListValueEventListener(reference: DatabaseReference): ValueEventListener {
        return object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val videoLessonList = extractor.extractVideoLessonList(dataSnapshot)
                onVideoLessonListAvailableListener?.onVideoLessonListAvailable(videoLessonList)
                reference.removeEventListener(this)
            }

            override fun onCancelled(databaseError: DatabaseError) {
                reference.removeEventListener(this)
            }
        }
    }
}