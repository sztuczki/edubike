package pl.fundacjakult.edubike.data_store.firebase

import pl.fundacjakult.edubike.features.video_lesson.OnVideoLessonListAvailableListener

interface DataStoreProvider {

    fun addOnDataAvailableListener(onDataAvailableListener: OnDataAvailableListener)
    fun addOnVideoLessonListAvailableListener(onVideoLessonListAvailableListener: OnVideoLessonListAvailableListener)

    fun attachVersionListener()
    fun attachLessonListListener()
    fun attachContentPartListListener()
    fun attachQuizQuestionListListener()
    fun attachVideoLessonListListener()
}