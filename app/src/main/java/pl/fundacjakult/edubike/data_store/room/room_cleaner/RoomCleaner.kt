package pl.fundacjakult.edubike.data_store.room.room_cleaner

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import pl.fundacjakult.edubike.framework.model.LessonContentPartFrameworkModel
import pl.fundacjakult.edubike.framework.model.LessonFrameworkModel
import pl.fundacjakult.edubike.framework.model.QuizQuestionFrameworkModel
import pl.fundacjakult.edubike.data_store.room.LocalDao

class RoomCleaner(private val localDao: LocalDao,
                  private val lifecycleOwner: LifecycleOwner,
                  var onRoomDataDeletedListener: OnRoomDataDeletedListener?) {

    private var lessonList = ArrayList<LessonFrameworkModel>()
    private var contentPartList = ArrayList<LessonContentPartFrameworkModel>()
    private var quizQuestionList = ArrayList<QuizQuestionFrameworkModel>()
    private var wereLessonsLoaded = false
    private var wereContentPartsLoaded = false
    private var wereQuizQuestionsLoaded = false

    fun clearLocalDatabase() {
        val liveDataLessonList = localDao.getAllLessons()
        liveDataLessonList.observe(lifecycleOwner, object : Observer<List<LessonFrameworkModel>> {
            override fun onChanged(lessons: List<LessonFrameworkModel>?) {
                this@RoomCleaner.lessonList = lessons as ArrayList<LessonFrameworkModel>
                wereLessonsLoaded = true
                tryClearingSQL()
                liveDataLessonList.removeObserver(this)
            }
        })

        val liveDataContentPartList = localDao.getAllContentParts()
        liveDataContentPartList.observe(lifecycleOwner, object : Observer<List<LessonContentPartFrameworkModel>> {
            override fun onChanged(contentParts: List<LessonContentPartFrameworkModel>?) {
                this@RoomCleaner.contentPartList = contentParts as ArrayList<LessonContentPartFrameworkModel>
                wereContentPartsLoaded = true
                tryClearingSQL()
                liveDataContentPartList.removeObserver(this)
            }
        })

        val liveDataQuizQuestionList = localDao.getAllQuizzes()
        liveDataQuizQuestionList.observe(lifecycleOwner, object : Observer<List<QuizQuestionFrameworkModel>> {
            override fun onChanged(quizQuestions: List<QuizQuestionFrameworkModel>?) {
                this@RoomCleaner.quizQuestionList = quizQuestions as ArrayList<QuizQuestionFrameworkModel>
                wereQuizQuestionsLoaded = true
                tryClearingSQL()
                liveDataQuizQuestionList.removeObserver(this)
            }
        })
    }

    private fun tryClearingSQL() {
        if (wereLessonsLoaded && wereContentPartsLoaded && wereQuizQuestionsLoaded) {
            doAsync {
                localDao.deleteContentParts(contentPartList)
                localDao.deleteQuizQuestions(quizQuestionList)
                localDao.deleteLessons(lessonList)
                uiThread {
                    onRoomDataDeletedListener!!.onRoomDataDeleted()
                }
            }
        }
    }
}