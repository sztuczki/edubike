package pl.fundacjakult.edubike.data_store.firebase

import pl.fundacjakult.edubike.framework.model.LessonContentPartFrameworkModel
import pl.fundacjakult.edubike.framework.model.LessonFrameworkModel
import pl.fundacjakult.edubike.framework.model.QuizQuestionFrameworkModel

interface OnDataAvailableListener {

    fun onDatabaseVersionAvailable(databaseVersion: String)
    fun onLessonListAvailable(lessonList: List<LessonFrameworkModel>)
    fun onContentPartListAvailable(contentPartList: List<LessonContentPartFrameworkModel>)
    fun onQuizQuestionListAvailable(quizQuestionList: List<QuizQuestionFrameworkModel>)
}