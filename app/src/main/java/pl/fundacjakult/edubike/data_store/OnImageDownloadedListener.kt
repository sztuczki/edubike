package pl.fundacjakult.edubike.data_store

import android.graphics.Bitmap

interface OnImageDownloadedListener {

    fun onImageDownloaded(bitmap: Bitmap?, filename: String)
}