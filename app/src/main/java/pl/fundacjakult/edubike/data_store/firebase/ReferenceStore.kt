package pl.fundacjakult.edubike.data_store.firebase

import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

private const val DATABASE_VERSION_REFERENCE = "database_version"
private const val LESSON_LIST_REFERENCE = "lesson_list"
private const val CONTENT_PART_LIST_REFERENCE = "content_part_list"
private const val QUIZ_QUESTION_LIST_REFERENCE = "quiz_question_list"
private const val VIDEO_LESSON_LIST_REFERENCE = "video_lesson_list"

class ReferenceStore(private val database: FirebaseDatabase) {

    fun getDatabaseVersionReference(): DatabaseReference {
        return database.reference.child(DATABASE_VERSION_REFERENCE)
    }

    fun getLessonListReference(): DatabaseReference {
        return database.reference.child(LESSON_LIST_REFERENCE)
    }

    fun getContentPartReference(): DatabaseReference {
        return database.reference.child(CONTENT_PART_LIST_REFERENCE)
    }

    fun getQuizQuestionReference(): DatabaseReference {
        return database.reference.child(QUIZ_QUESTION_LIST_REFERENCE)
    }

    fun getVideoLessonListReference(): DatabaseReference {
        return database.reference.child(VIDEO_LESSON_LIST_REFERENCE)
    }
}