package pl.fundacjakult.edubike.data_store.room

import androidx.lifecycle.LiveData
import androidx.room.*
import pl.fundacjakult.edubike.framework.model.*

@Dao
interface LocalDao {

    @Query("SELECT * FROM lesson_list")
    fun getAllLessons(): LiveData<List<LessonFrameworkModel>>

    @Query("SELECT * FROM content_part_list")
    fun getAllContentParts(): LiveData<List<LessonContentPartFrameworkModel>>

    @Query("SELECT * FROM quiz_question_list")
    fun getAllQuizzes(): LiveData<List<QuizQuestionFrameworkModel>>

    @Query("SELECT * FROM content_part_list WHERE lesson = :lessonId")
    fun getLessonContentParts(lessonId: String): LiveData<List<LessonContentPartFrameworkModel>>

    @Query("SELECT * FROM quiz_question_list WHERE lesson = :lessonId")
    fun getLessonQuizzes(lessonId: String): LiveData<List<QuizQuestionFrameworkModel>>

//    @Query("SELECT * FROM solved_lesson_list")
//    fun getSolvedLessonList(): LiveData<List<CompletedLessonFrameworkModel>>
//
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    fun insertSolvedLesson(solvedLesson: CompletedLessonFrameworkModel)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(lessons: List<LessonFrameworkModel>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLessonContentParts(contentParts: List<LessonContentPartFrameworkModel>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLessonQuizzes(quizQuestions: List<QuizQuestionFrameworkModel>)

    @Delete
    fun deleteLessons(lessonList: List<LessonFrameworkModel>)

    @Delete
    fun deleteContentParts(contentPartList: List<LessonContentPartFrameworkModel>)

    @Delete
    fun deleteQuizQuestions(quizQuestionList: List<QuizQuestionFrameworkModel>)
}