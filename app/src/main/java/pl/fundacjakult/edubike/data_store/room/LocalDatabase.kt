package pl.fundacjakult.edubike.data_store.room

import androidx.room.Database
import androidx.room.RoomDatabase
import pl.fundacjakult.edubike.framework.model.*

@Database(entities = [LessonFrameworkModel::class,
    LessonContentPartFrameworkModel::class,
    QuizQuestionFrameworkModel::class,
    CompletedLessonFrameworkModel::class], version = 1, exportSchema = false)
abstract class LocalDatabase : RoomDatabase() {

    abstract fun getLocalDao(): LocalDao
    abstract fun getRxLocalDao(): RxLocalDao
}