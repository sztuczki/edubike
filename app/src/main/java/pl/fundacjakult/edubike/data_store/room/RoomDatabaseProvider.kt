package pl.fundacjakult.edubike.data_store.room

interface RoomDatabaseProvider {

    fun getLocalDao(): LocalDao
    fun getRxLocalDao(): RxLocalDao
}