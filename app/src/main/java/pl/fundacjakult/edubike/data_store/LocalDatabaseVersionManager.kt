package pl.fundacjakult.edubike.data_store

import android.content.Context
import android.preference.PreferenceManager

class LocalDatabaseVersionManager(private val context: Context) {

    fun onDataDownloadingFailed() {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("FirebaseDatabaseVersion", "Downloading Error").apply()
    }

    fun onConnectionRequired() {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("FirebaseDatabaseVersion", "Connection Required").apply()
    }

    fun onLocalDataDeleted() {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("FirebaseDatabaseVersion", "Deleted").apply()
    }

    fun onLocalDataSuccessfullySaved(databaseVersion: String) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("FirebaseDatabaseVersion", databaseVersion).apply()
    }

    fun hasLocalDatabase(): Boolean {
        return PreferenceManager.getDefaultSharedPreferences(context).contains("FirebaseDatabaseVersion")
    }

    fun getLocalDatabaseVersion(): String {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("FirebaseDatabaseVersion", null)
    }
}