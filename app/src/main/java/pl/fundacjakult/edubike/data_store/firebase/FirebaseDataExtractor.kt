package pl.fundacjakult.edubike.data_store.firebase

import com.google.firebase.database.DataSnapshot
import pl.fundacjakult.edubike.framework.model.LessonContentPartFrameworkModel
import pl.fundacjakult.edubike.framework.model.LessonFrameworkModel
import pl.fundacjakult.edubike.framework.model.QuizQuestionFrameworkModel
import pl.fundacjakult.edubike.features.video_lesson.VideoLesson

class FirebaseDataExtractor {

    fun extractLessonList(dataSnapshot: DataSnapshot): ArrayList<LessonFrameworkModel> {
        val lessonList = ArrayList<LessonFrameworkModel>()
        val dataIterable = dataSnapshot.children
        for (lessonData in dataIterable) {
            val lesson = lessonData.getValue(LessonFrameworkModel::class.java)
            lessonList.add(lesson!!)
        }
        return lessonList
    }

    fun extractContentPartList(dataSnapshot: DataSnapshot): ArrayList<LessonContentPartFrameworkModel> {
        val contentPartList = ArrayList<LessonContentPartFrameworkModel>()
        val dataIterable = dataSnapshot.children
        for (lesson in dataIterable) {
            val lessonIterable = lesson.children
            for (contentData in lessonIterable) {
                val contentPart = contentData.getValue(LessonContentPartFrameworkModel::class.java)
                contentPartList.add(contentPart!!)
            }
        }
        return contentPartList
    }

    fun extractQuizQuestionList(dataSnapshot: DataSnapshot): ArrayList<QuizQuestionFrameworkModel> {
        val quizQuestionList = ArrayList<QuizQuestionFrameworkModel>()
        val dataIterable = dataSnapshot.children
        for (lesson in dataIterable) {
            val lessonIterable = lesson.children
            for (quizData in lessonIterable) {
                val quizQuestion = quizData.getValue(QuizQuestionFrameworkModel::class.java)
                quizQuestionList.add(quizQuestion!!)
            }
        }
        return quizQuestionList
    }

    fun extractVideoLessonList(dataSnapshot: DataSnapshot): ArrayList<VideoLesson> {
        val videoLessonList = ArrayList<VideoLesson>()
        val dataIterable = dataSnapshot.children
        for (videoLessonData in dataIterable) {
            val videoLesson = videoLessonData.getValue(VideoLesson::class.java)
            videoLessonList.add(videoLesson!!)
        }
        return videoLessonList
    }
}