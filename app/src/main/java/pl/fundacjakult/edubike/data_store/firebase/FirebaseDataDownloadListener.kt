package pl.fundacjakult.edubike.data_store.firebase

interface FirebaseDataDownloadListener {

    fun onFirebaseDataDownloadingStarted()
    fun onFirebaseDataDownloadingFailed()
    fun onFirebaseDataDownloaded()
    fun onConnectionRequired()
}