package pl.fundacjakult.edubike.image_utils

import android.content.Context
import android.graphics.Bitmap
import com.bumptech.glide.Glide
import pl.fundacjakult.edubike.data_store.OnAllImagesDownloadedListener
import pl.fundacjakult.edubike.data_store.OnImageDownloadedListener
import pl.fundacjakult.edubike.data_store.firebase.FirebaseDataDownloadListener
import java.util.concurrent.ExecutionException

class ImageLoader(private val context: Context,
                  private val imageLoaderThread: ImageLoaderThread) {

    var onAllImagesDownloadedListener: OnAllImagesDownloadedListener? = null
    var onImageDownloadedListener: OnImageDownloadedListener? = null
    var firebaseDataDownloadListener: FirebaseDataDownloadListener? = null
    var imagesToLoad = 0
    var imagesLoaded = 0

    init {
        imageLoaderThread.start()
//        DataRepository()
    }

    fun loadImage(imageUrl: String, fileName: String) {
        imageLoaderThread.postTask {
            val bitmap: Bitmap?
            try {
                bitmap = Glide.with(context)
                        .load(imageUrl)
                        .asBitmap()
                        .into(1280, 720)
                        .get()
                onImageDownloadedListener?.onImageDownloaded(bitmap, fileName)
            } catch (exception: InterruptedException) {
                exception.printStackTrace()
                firebaseDataDownloadListener?.onFirebaseDataDownloadingFailed()
                firebaseDataDownloadListener = null
            } catch (exception: ExecutionException) {
                exception.printStackTrace()
                firebaseDataDownloadListener?.onFirebaseDataDownloadingFailed()
                firebaseDataDownloadListener = null
            }
            imagesLoaded = imagesLoaded.inc()
            if (imagesLoaded == imagesToLoad) {
                onAllImagesDownloadedListener!!.onAllImagesDownloaded()
                imagesToLoad = 0
                imagesLoaded = 0
            }
        }
    }
}