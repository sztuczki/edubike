package pl.fundacjakult.edubike.image_utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

import java.io.File;

public class ImageProvider {

    private File imagesFile;

    public ImageProvider(Context context) {
        imagesFile = new File(context.getFilesDir(), "images");
    }

    public void loadInto(ImageView imageView, String filename) {
        File imageFile = new File(imagesFile, filename);
        Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
        imageView.setImageBitmap(bitmap);
    }
}