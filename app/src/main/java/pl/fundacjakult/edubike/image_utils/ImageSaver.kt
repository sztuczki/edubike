package pl.fundacjakult.edubike.image_utils

import android.content.Context
import android.graphics.Bitmap
import pl.fundacjakult.edubike.data_store.firebase.FirebaseDataDownloadListener
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException

class ImageSaver(context: Context, private val imageSaverThread: ImageSaverThread) {

    private val IMAGES_DIRECTORY = "images"
    private val imagesFile = File(context.filesDir, IMAGES_DIRECTORY)

    lateinit var firebaseDataDownloadListener: FirebaseDataDownloadListener

    init {
        imagesFile.mkdir()
        imageSaverThread.start()
    }

    fun saveImageToFile(bitmap: Bitmap?, filename: String) {
        imageSaverThread.postTask { trySaveImage(bitmap, filename) }
    }

    private fun trySaveImage(bitmap: Bitmap?, filename: String) {
        try {
            val newImageFile = File(imagesFile, filename)
            val outputStream = FileOutputStream(newImageFile)
            if (bitmap != null) {
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
                try {
                    outputStream.close()
                } catch (exception: IOException) {
                    exception.printStackTrace()
                }
            } else {
                firebaseDataDownloadListener.onFirebaseDataDownloadingFailed()
            }
        } catch (exception: FileNotFoundException) {
            exception.printStackTrace()
        } catch (exception: NullPointerException) {
            firebaseDataDownloadListener.onFirebaseDataDownloadingFailed()
        }
    }
}