package pl.fundacjakult.edubike.image_utils

import android.os.Handler
import android.os.HandlerThread

class ImageLoaderThread : HandlerThread("ImageLoaderThread") {

    private var handler: Handler? = null

    override fun onLooperPrepared() {
        super.onLooperPrepared()
        handler = Handler(looper)
    }

    fun postTask(action: () -> Unit) {
        val task = Runnable(action)
        handler?.post(task)
    }
}