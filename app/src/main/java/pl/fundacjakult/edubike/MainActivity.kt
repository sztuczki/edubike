package pl.fundacjakult.edubike

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.facebook.stetho.Stetho
import pl.fundacjakult.edubike.framework.dependency_injection.components.MainActivityComponent
import pl.fundacjakult.edubike.framework.dependency_injection.components.DaggerMainActivityComponent
import pl.fundacjakult.edubike.framework.dependency_injection.modules.ActivityModule
import pl.fundacjakult.edubike.navigation_utils.NavigationProvider
import pl.fundacjakult.edubike.navigation_utils.Navigator

class MainActivity : AppCompatActivity(), NavigationProvider {

    private var navigator: Navigator? = null

    companion object {
        var mainActivityComponent: MainActivityComponent? = null
    }

    init {
        mainActivityComponent = DaggerMainActivityComponent.builder()
                .activityModule(ActivityModule(this))
                .build()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            getNavigator().navigateToMenuFragment()
            Stetho.initializeWithDefaults(applicationContext)
        }
    }

    override fun onBackPressed() {
        if (getNavigator().isBackStackEmpty()) {
            super.onBackPressed()
        } else {
            getNavigator().navigateBack()
        }
    }

    override fun getNavigator(): Navigator {
        return navigator ?: Navigator(supportFragmentManager).also { navigator = it }
    }
}