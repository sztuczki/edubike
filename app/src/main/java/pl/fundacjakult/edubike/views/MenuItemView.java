package pl.fundacjakult.edubike.views;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import pl.fundacjakult.edubike.R;

public class MenuItemView extends ConstraintLayout {

    AppCompatImageView menuItemImageView;
    AppCompatTextView menuItemHeader;
    AppCompatTextView menuItemSubHeader;
    AppCompatTextView menuItemButton;

    public MenuItemView(Context context) {
        super(context);
        init();
    }

    public MenuItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MenuItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.menu_item, this);
        menuItemImageView = findViewById(R.id.menu_item_drawable);
        menuItemHeader = findViewById(R.id.menu_item_header);
        menuItemSubHeader = findViewById(R.id.menu_item_sub_header);
        menuItemButton = findViewById(R.id.menu_item_button);
    }

    public void updateView(int imageResource, String headerText, String subHeaderText, String buttonText) {
        menuItemImageView.setImageResource(imageResource);
        menuItemHeader.setText(headerText);
        menuItemSubHeader.setText(subHeaderText);
        menuItemButton.setText(buttonText);
    }

    public void setOnButtonClickListener(OnClickListener onClickListener) {
        this.setOnClickListener(onClickListener);
    }
}