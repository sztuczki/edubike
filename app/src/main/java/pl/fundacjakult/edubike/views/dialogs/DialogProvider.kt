package pl.fundacjakult.edubike.views.dialogs

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog

class DialogProvider {

    companion object {
        fun showNeutralDialog(context: Context, message: String, positive: String, onClick: () -> Unit) {
            val onClickListener = DialogInterface.OnClickListener { _, _ -> onClick() }
            val dialog = AlertDialog.Builder(context)
                    .setMessage(message)
                    .setPositiveButton(positive, onClickListener)
                    .create()
            dialog.show()
        }

        fun showDialog(context: Context, message: String, positive: String, negative: String, onPositiveClick: () -> Unit, onNegativeClick: () -> Unit) {
            val onPositiveClickListener = DialogInterface.OnClickListener { dialog, _ ->
                onPositiveClick()
                dialog?.dismiss()
            }
            val onNegativeClickListener = DialogInterface.OnClickListener { dialog, _ ->
                onNegativeClick()
                dialog?.dismiss()
            }
            val dialog = AlertDialog.Builder(context)
                    .setMessage(message)
                    .setPositiveButton(positive, onPositiveClickListener)
                    .setNegativeButton(negative, onNegativeClickListener)
                    .create()
            dialog.show()
        }
    }
}