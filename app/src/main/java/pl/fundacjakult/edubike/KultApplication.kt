package pl.fundacjakult.edubike

import android.app.Application
import androidx.room.Room
import pl.fundacjakult.edubike.data_store.room.*
import pl.fundacjakult.edubike.framework.dependency_injection.components.ApplicationComponent
import pl.fundacjakult.edubike.framework.dependency_injection.components.DaggerApplicationComponent
import pl.fundacjakult.edubike.framework.dependency_injection.modules.RepositoryModule

class KultApplication : Application(), RoomDatabaseProvider {

    private lateinit var localDao: LocalDao
    private lateinit var rxLocalDao: RxLocalDao

    companion object {
        var component: ApplicationComponent? = null
    }

    override fun onCreate() {
        super.onCreate()
        setupRoomDatabase()
        setupApplicationComponent(rxLocalDao)
    }

    override fun getLocalDao(): LocalDao {
        return localDao
    }

    override fun getRxLocalDao(): RxLocalDao {
        return rxLocalDao
    }

    private fun setupRoomDatabase() {
        val localDatabase = Room.databaseBuilder(applicationContext, LocalDatabase::class.java, "lesson_database").build()
        localDao = localDatabase.getLocalDao()
        rxLocalDao = localDatabase.getRxLocalDao()
        setupApplicationComponent(rxLocalDao)
    }

    private fun setupApplicationComponent(localDao: RxLocalDao) {
        component = DaggerApplicationComponent.builder()
                .repositoryModule(RepositoryModule(RoomDataStore(localDao)))
                .build()
    }
}