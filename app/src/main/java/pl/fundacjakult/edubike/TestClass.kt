package pl.fundacjakult.edubike

fun Int.foo(x: Int): Int {
    return this + x
}

class TestClass {
    val asd = 5

    companion object {
        var someVariable: String = "setupComponent"
    }

    inline fun foo(someFun: () -> Int) {

    }

    fun bar(): Int {
        someVariable
        return 5
    }
}