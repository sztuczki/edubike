package pl.fundacjakult.edubike.features.video_lesson;

import com.google.firebase.database.PropertyName;

import androidx.annotation.NonNull;

public class VideoLesson {

    @PropertyName("lesson_id")
    public String lessonId;
    public String name;
    @PropertyName("video_id")
    public String videoId;

    public VideoLesson() {
    }

    public VideoLesson(@NonNull String lessonId, @NonNull String name, @NonNull String videoId) {
        this.lessonId = lessonId;
        this.name = name;
        this.videoId = videoId;
    }
}