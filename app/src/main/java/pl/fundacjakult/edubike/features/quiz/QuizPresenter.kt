package pl.fundacjakult.edubike.features.quiz

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.schedulers.Schedulers
import pl.fundacjakult.edubike.data_store.room.LocalDao
import pl.fundacjakult.edubike.data_store.room.RxLocalDao
import pl.fundacjakult.edubike.framework.model.QuizQuestionFrameworkModel
import pl.fundacjakult.edubike.framework.model.CompletedLessonFrameworkModel
import pl.fundacjakult.edubike.navigation_utils.Navigator

class QuizPresenter(private val rxLocalDao: RxLocalDao,
                    private val localDao: LocalDao,
                    val lessonId: String?,
                    private val navigator: Navigator,
                    private val view: QuizContract.View,
                    private val quizRoulette: QuizRoulette) : QuizContract.Presenter {

    private var unpackingLessonId: String? = null
    private var unpackingLessonQuizzes: ArrayList<QuizQuestionFrameworkModel>? = null
    private var drawnQuestionList = ArrayList<QuizQuestionFrameworkModel>()
    private val activeQuestionData = MediatorLiveData<QuizQuestionFrameworkModel>()
    private var activeQuestionIndex = 0
    private var score = 0

    init {
        startListeningForSource()
    }

    private fun startListeningForSource() {
        if (lessonId != null) {
            activeQuestionData.addSource(localDao.getLessonQuizzes(lessonId)) { quizQuestionList ->
                drawQuizQuestions(quizQuestionList)
                activeQuestionData.postValue(getActiveQuizQuestion())
            }
        } else {
            activeQuestionData.addSource(localDao.getAllQuizzes()) { quizQuestionList ->
                drawBigQuizQuestions(quizQuestionList)
                activeQuestionData.postValue(getActiveQuizQuestion())
            }
        }
    }

    fun getActiveQuestionData(): LiveData<QuizQuestionFrameworkModel> {
        return activeQuestionData
    }

    private fun drawQuizQuestions(questionList: List<QuizQuestionFrameworkModel>) {
        quizRoulette.addQuestionsForLesson(questionList)
        drawnQuestionList = quizRoulette.pickQuizQuestions(5) as ArrayList<QuizQuestionFrameworkModel>
    }

    private fun drawBigQuizQuestions(allQuestionList: List<QuizQuestionFrameworkModel>) {
        for (question in allQuestionList) {
            if (unpackingLessonId == null) {
                unpackingLessonId = question.lesson
                unpackingLessonQuizzes = ArrayList()
            }
            if (unpackingLessonId != question.lesson) {
                quizRoulette.addQuestionsForLesson(unpackingLessonQuizzes)
                unpackingLessonQuizzes = ArrayList()
                unpackingLessonQuizzes!!.add(question)
                unpackingLessonId = question.lesson
            } else {
                unpackingLessonQuizzes!!.add(question)
            }
        }
        quizRoulette.addQuestionsForLesson(unpackingLessonQuizzes)
        drawnQuestionList = quizRoulette.pickQuizQuestions(5) as ArrayList<QuizQuestionFrameworkModel>
    }

    private fun getActiveQuizQuestion(): QuizQuestionFrameworkModel {
        return drawnQuestionList[activeQuestionIndex]
    }

    fun onNextQuestionClicked() {
        activeQuestionIndex = activeQuestionIndex.inc()
        if (activeQuestionIndex < drawnQuestionList.size) {
            activeQuestionData.postValue(getActiveQuizQuestion())
        } else {
            view.showQuizSummary(score)
        }
    }

    fun onPreviousQuestionClicked() {
        if (hasPrevQuizQuestion()) {
            activeQuestionIndex = activeQuestionIndex.dec()
            if (activeQuestionIndex >= 0) {
                activeQuestionData.postValue(getActiveQuizQuestion())
            } else {
                activeQuestionIndex = 0
            }
        }
    }

    fun submitAnswer(answer: String) {
        getActiveQuizQuestion().chosenAnswer = answer
        if (getActiveQuizQuestion().answer == getActiveQuizQuestion().chosenAnswer) {
            score = score.inc()
        }
        activeQuestionData.postValue(getActiveQuizQuestion())
    }

    fun wasPassed(): Boolean {
        return score > 3
    }

    fun saveResult(lessonId: String) {
//        val solvedLesson = CompletedLessonFrameworkModel()
//        solvedLesson.setId(lessonId)
//        doAsync { localDao.insertSolvedLesson(solvedLesson) }
        val solvedLesson = CompletedLessonFrameworkModel()
        solvedLesson.id = lessonId
        val disposable = rxLocalDao.saveSolvedLesson(solvedLesson)
                .subscribeOn(Schedulers.computation())
                .subscribeWith(object : DisposableCompletableObserver() {
                    override fun onComplete() {
//                        returnToLessonListFragment()
                        dispose()
                        Log.v("setupComponent", "setupComponent")
                    }

                    override fun onError(e: Throwable) {
                        Log.v("setupComponent", "setupComponent")
                    }
                })

    }

    fun shouldEnableNextQuestionButton(): Boolean {
        return hasNextQuizQuestion() && getActiveQuizQuestion().chosenAnswer != null
    }

    fun shouldEnablePrevQuestionButton(): Boolean {
        return hasPrevQuizQuestion()
    }

    private fun hasNextQuizQuestion(): Boolean {
        return activeQuestionIndex + 1 <= drawnQuestionList.size
    }

    private fun hasPrevQuizQuestion(): Boolean {
        return activeQuestionIndex > 0
    }

    fun returnToLessonListFragment() {
        navigator.returnToLessonListFragment()
    }
}