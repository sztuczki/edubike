package pl.fundacjakult.edubike.features.video_lesson;

import java.util.List;

public interface OnVideoLessonListAvailableListener {

    void onVideoLessonListAvailable(List<VideoLesson> videoLessonList);
}
