package pl.fundacjakult.edubike.features.video_lesson.youtube_player;

import pl.fundacjakult.edubike.BuildConfig;

public class YouTubeConfig {

    private static final String API_KEY = BuildConfig.youtubeApiKey;

    public YouTubeConfig() {
    }

    public static String getYouTubeApiKey() {
        return API_KEY;
    }
}