package pl.fundacjakult.edubike.features.lesson_list

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import pl.fundacjakult.edubike.domain.model.LessonModel
import pl.fundacjakult.edubike.navigation_utils.Navigator
import pl.fundacjakult.edubike.usecases.GetLessonListWithStatus

class LessonListPresenter(
        private val view: LessonListContract.View,
        private val navigator: Navigator,
        private val getLessonListWithStatus: GetLessonListWithStatus,
        private val compositeDisposable: CompositeDisposable = CompositeDisposable()) : LessonListContract.Presenter {

    override fun observeLessonData() {
        getLessonListWithStatus()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<List<LessonModel>>() {
                    override fun onSuccess(t: List<LessonModel>) {
                        view.onLessonDataUpdated(t)
                    }

                    override fun onError(e: Throwable) {}
                }).also { compositeDisposable.add(it) }
    }

    override fun navigateToLessonFragment(lesson: LessonModel) {
        navigator.navigateToLessonFragment(lesson)
    }

    override fun navigateToBigQuizFragment() {
        navigator.navigateToBigQuizFragment()
    }

    override fun onStop() {
        compositeDisposable.clear()
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
    }
}