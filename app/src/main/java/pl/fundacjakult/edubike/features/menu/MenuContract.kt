package pl.fundacjakult.edubike.features.menu

interface MenuContract {

    interface View {
        fun shouldShowSplash(shouldShowSplash: Boolean)
        fun hideSplashScreen()
        fun updateSplashMessage(message: String)
        fun showConnectionRequiredDialog()
        fun showConnectionInterruptedDialog()
    }

    interface Presenter {
        fun navigateToLessonListFragment()
        fun navigateToVideoLessonListFragment()
        fun navigateToEventsFragment()
        fun navigateToPartnersFragment()
        fun navigateToPrivacyPolicyWebsite()
    }
}