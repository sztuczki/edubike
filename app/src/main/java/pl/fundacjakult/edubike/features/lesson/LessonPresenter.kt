package pl.fundacjakult.edubike.features.lesson

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import pl.fundacjakult.edubike.domain.model.LessonContentPartModel
import pl.fundacjakult.edubike.navigation_utils.Navigator
import pl.fundacjakult.edubike.usecases.GetContentPartsForLesson

class LessonPresenter(private val view: LessonContract.View,
                      private val getContentPartsForLesson: GetContentPartsForLesson,
                      private val navigator: Navigator,
                      private val compositeDisposable: CompositeDisposable = CompositeDisposable()) : LessonContract.Presenter {

    override fun observeLessonContents(lessonId: String) {
        getContentPartsForLesson(lessonId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<List<LessonContentPartModel>>() {
                    override fun onSuccess(t: List<LessonContentPartModel>) {
                        view.updateFor(t)
                    }

                    override fun onError(e: Throwable) {
                    }
                }).also { compositeDisposable.add(it) }
    }

    override fun navigateToQuizFragment(lessonId: String) {
        navigator.navigateToQuizFragment(lessonId)
    }

    override fun onStop() {
        compositeDisposable.clear()
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
    }
}