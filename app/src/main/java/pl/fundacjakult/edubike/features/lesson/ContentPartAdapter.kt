package pl.fundacjakult.edubike.features.lesson

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.item_content_part.view.*
import pl.fundacjakult.edubike.R
import pl.fundacjakult.edubike.domain.model.LessonContentPartModel
import pl.fundacjakult.edubike.framework.model.LessonContentPartFrameworkModel
import pl.fundacjakult.edubike.image_utils.ImageProvider

class ContentPartAdapter(private val imageProvider: ImageProvider,
                         private var contentPartList: ArrayList<LessonContentPartModel> = ArrayList()) : RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return ContentPartViewHolder(view)
    }

    override fun getItemCount(): Int {
        return contentPartList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        (holder as ContentPartViewHolder).bindData(contentPartList[position])
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_content_part
    }

    inner class ContentPartViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val contentTextView = itemView.lesson_content_text_view
        private val contentImageView = itemView.lesson_content_image_view
        private val captionTextView = itemView.lesson_content_image_caption

        fun bindData(contentPart: LessonContentPartModel) {
            if (isImage(contentPart)) {
                setImageView()
                imageProvider.loadInto(contentImageView, contentPart.filename)
                if (contentPart.caption != null) {
                    captionTextView.text = contentPart.caption
                    captionTextView.visibility = View.VISIBLE
                } else {
                    captionTextView.height = 0
                    captionTextView.visibility = View.GONE
                }
            } else if (isText(contentPart)) {
                setTextView()
                contentTextView.text = HtmlCompat.fromHtml(contentPart.data, HtmlCompat.FROM_HTML_MODE_LEGACY)
            }
        }

        private fun isImage(contentPart: LessonContentPartModel): Boolean {
            return contentPart.type == "imageUrl"
        }

        private fun isText(contentPart: LessonContentPartModel): Boolean {
            return contentPart.type == "text"
        }

        private fun setTextView() {
            contentTextView.visibility = View.VISIBLE
            contentImageView.visibility = View.GONE
            captionTextView.visibility = View.GONE
        }

        private fun setImageView() {
            contentImageView.visibility = View.VISIBLE
            contentTextView.visibility = View.GONE
        }
    }

    fun updateContentPartList(list: List<LessonContentPartModel>) {
        this.contentPartList.clear()
        this.contentPartList = list as ArrayList<LessonContentPartModel>
        notifyDataSetChanged()
    }
}