package pl.fundacjakult.edubike.features.menu

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.transition.Slide
import android.transition.Transition
import android.transition.TransitionManager
import android.view.*
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.fragment_menu.*
import kotlinx.android.synthetic.main.splash_screen.view.*
import org.jetbrains.anko.connectivityManager
import org.jetbrains.anko.support.v4.runOnUiThread
import pl.fundacjakult.edubike.R
import pl.fundacjakult.edubike.compliance.PrivacyPopupManager
import pl.fundacjakult.edubike.connectivity_utils.NetworkMonitor
import pl.fundacjakult.edubike.data_store.*
import pl.fundacjakult.edubike.data_store.firebase.DataStore
import pl.fundacjakult.edubike.data_store.firebase.ReferenceStore
import pl.fundacjakult.edubike.data_store.room.LocalDao
import pl.fundacjakult.edubike.data_store.room.RoomDatabaseProvider
import pl.fundacjakult.edubike.data_store.room.room_cleaner.RoomCleaner
import pl.fundacjakult.edubike.image_utils.ImageLoader
import pl.fundacjakult.edubike.image_utils.ImageLoaderThread
import pl.fundacjakult.edubike.image_utils.ImageSaver
import pl.fundacjakult.edubike.image_utils.ImageSaverThread
import pl.fundacjakult.edubike.navigation_utils.NavigationProvider
import pl.fundacjakult.edubike.navigation_utils.Navigator
import pl.fundacjakult.edubike.navigation_utils.WebBrowserIntentProvider
import pl.fundacjakult.edubike.views.dialogs.DialogProvider

class MenuFragment : Fragment(), MenuContract.View, Toolbar.OnMenuItemClickListener {

    private lateinit var imageSaverThread: ImageSaverThread
    private lateinit var imageLoaderThread: ImageLoaderThread
    private lateinit var menuPresenter: MenuPresenter
    private lateinit var splashScreenView: View
    private lateinit var privacyPopupManager: PrivacyPopupManager

    private var showSplash = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState != null) showSplash = false
        privacyPopupManager = PrivacyPopupManager(getAppContext()!!)
        imageLoaderThread = ImageLoaderThread()
        imageSaverThread = ImageSaverThread()
        val imageLoader = ImageLoader(getAppContext()!!, imageLoaderThread)
        val imageSaver = ImageSaver(getAppContext()!!, imageSaverThread)
        val localDatabaseVersionManager = LocalDatabaseVersionManager(getAppContext()!!)
        val browserIntentProvider = WebBrowserIntentProvider(activity!!)
        val roomCleaner = RoomCleaner(getLocalDao(), this, null)
        val connectivityManager = getAppContext()!!.connectivityManager
        val networkInfoProvider = NetworkMonitor(connectivityManager)
        val database = FirebaseDatabase.getInstance()
        val referenceStore = ReferenceStore(database)
        val dataStore = DataStore(referenceStore)

        menuPresenter = MenuPresenter(this,
                localDatabaseVersionManager,
                getLocalDao(),
                imageLoader,
                imageSaver,
                getNavigator(),
                browserIntentProvider,
                roomCleaner,
                networkInfoProvider,
                dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        inflateSplash()
        setupToolbar()
        updateViews()
    }

    override fun onResume() {
        super.onResume()
        menuPresenter.checkForDatabaseUpdates()
    }

    private fun inflateSplash() {
        if (showSplash) {
            menu_stub.layoutResource = R.layout.splash_screen
            splashScreenView = menu_stub.inflate()
        } else {
            setOnMenuItemClickListeners()
        }
    }

    private fun setupToolbar() {
        (menu_toolbar as Toolbar).inflateMenu(R.menu.menu_main)
        (menu_toolbar as Toolbar).setOnMenuItemClickListener(this)
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.privacy_policy -> {
                menuPresenter.navigateToPrivacyPolicyWebsite()
                true
            }
            else -> {
                false
            }
        }
    }

    private fun updateViews() {
        traffic_regulations_menu_item.updateView(R.drawable.swiatla,
                getString(R.string.traffic_regulations_header),
                getString(R.string.traffic_regulations_description),
                getString(R.string.traffic_regulations_cta))
        riding_techniques_menu_item.updateView(R.drawable.rower,
                getString(R.string.riding_techniques_header),
                getString(R.string.riding_techniques_description),
                getString(R.string.general_cta))
        events_menu_item.updateView(R.drawable.namiot,
                getString(R.string.events_header),
                getString(R.string.events_description),
                getString(R.string.general_cta))
        partners_menu_item.updateView(R.drawable.sztama,
                getString(R.string.partners_header),
                getString(R.string.partners_description),
                getString(R.string.general_cta))
    }

    private fun setOnMenuItemClickListeners() {
        traffic_regulations_menu_item.setOnButtonClickListener { menuPresenter.navigateToLessonListFragment() }
        riding_techniques_menu_item.setOnButtonClickListener { menuPresenter.navigateToVideoLessonListFragment() }
        events_menu_item.setOnButtonClickListener { menuPresenter.navigateToEventsFragment() }
        partners_menu_item.setOnButtonClickListener { menuPresenter.navigateToPartnersFragment() }
    }

    override fun shouldShowSplash(shouldShowSplash: Boolean) {
        showSplash = shouldShowSplash
    }

    override fun hideSplashScreen() {
        if (activity != null && showSplash) {
            runOnUiThread {
                showSplash = false
                val slide = Slide(Gravity.START)
                TransitionManager.beginDelayedTransition(menu_layout, slide)
                splashScreenView.visibility = View.INVISIBLE
                setOnMenuItemClickListeners()
                showPrivacyPopup()
            }
        }
    }

    override fun updateSplashMessage(message: String) {
        animateOldMessageOut {
            val runnable = Runnable { animateNewMessageIn(message) }
            Handler().postDelayed(runnable, 300)
        }
    }

    private fun showPrivacyPopup() {
        if (!privacyPopupManager.wasPrivacyPopupDisplayed()) {
            DialogProvider.showDialog(context!!, "Czy chcesz zapoznać się z naszą polityką prywatności? Zawsze możesz zrobić to później w opcjach głównego menu.", "Pokaż", "Nie teraz", {
                privacyPopupManager.onPrivacyPopupDisplayed()
                menuPresenter.navigateToPrivacyPolicyWebsite()
            }, {
                privacyPopupManager.onPrivacyPopupDisplayed()
            })
        }
    }

    private fun animateOldMessageOut(onTransitionEnded: () -> Unit) {
        val slideOut = Slide(Gravity.END)
        slideOut.interpolator = AccelerateInterpolator()
        slideOut.startDelay = 300
        slideOut.duration = 300
        slideOut.addListener(object : Transition.TransitionListener {
            override fun onTransitionEnd(transition: Transition?) {
                Handler().post { onTransitionEnded() }
            }

            override fun onTransitionResume(transition: Transition?) {
            }

            override fun onTransitionPause(transition: Transition?) {
            }

            override fun onTransitionCancel(transition: Transition?) {
            }

            override fun onTransitionStart(transition: Transition?) {
            }
        })
        TransitionManager.beginDelayedTransition(menu_layout, slideOut)
        splashScreenView.splash_info_text_view.visibility = View.INVISIBLE
    }

    private fun animateNewMessageIn(newMessage: String) {
        val slideIn = Slide(Gravity.START)
        slideIn.interpolator = DecelerateInterpolator()
        slideIn.duration = 300
        TransitionManager.beginDelayedTransition(menu_layout, slideIn)
        splashScreenView.splash_info_text_view.text = newMessage
        splashScreenView.splash_info_text_view.visibility = View.VISIBLE

    }

    override fun showConnectionRequiredDialog() {
        runOnUiThread {
            val message = resources.getString(R.string.splash_dialog_connection_required)
            val neutralString = resources.getString(R.string.ok)
            if (activity != null) DialogProvider.showNeutralDialog(activity!!, message, neutralString) {
                activity?.finishAndRemoveTask()
            }
        }
    }

    override fun showConnectionInterruptedDialog() {
        runOnUiThread {
            val message = getString(R.string.splash_screen_downloading_error)
            val neutralString = getString(R.string.ok)
            if (activity != null) DialogProvider.showNeutralDialog(activity!!, message, neutralString) {
                activity?.finishAndRemoveTask()
            }
        }
    }

    override fun onDestroy() {
        imageLoaderThread.quitSafely()
        imageSaverThread.quitSafely()
        super.onDestroy()
    }

    private fun getAppContext(): Context? {
        return activity?.applicationContext
    }

    private fun getLocalDao(): LocalDao {
        return (activity?.application as RoomDatabaseProvider).getLocalDao()
    }

    private fun getNavigator(): Navigator {
        return (activity as NavigationProvider).getNavigator()
    }
}