package pl.fundacjakult.edubike.features.lesson_list

import pl.fundacjakult.edubike.domain.model.LessonModel

interface LessonListContract {

    interface View {
        fun onLessonDataUpdated(lessonList: List<LessonModel>)
    }

    interface Presenter {
        fun observeLessonData()

        fun onStop()
        fun onDestroy()

        fun navigateToLessonFragment(lesson: LessonModel)
        fun navigateToBigQuizFragment()
    }
}