package pl.fundacjakult.edubike.features.video_lesson;

import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import pl.fundacjakult.edubike.data_store.firebase.DataStore;
import pl.fundacjakult.edubike.data_store.firebase.ReferenceStore;

public class VideoLessonListPresenter implements VideoLessonContract.Presenter, OnVideoLessonListAvailableListener {

    private DataStore firebaseDataStore;
    private MediatorLiveData<List<VideoLesson>> videoLessonListMediatorLiveData = new MediatorLiveData<>();


    VideoLessonListPresenter() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        ReferenceStore store = new ReferenceStore(database);
        firebaseDataStore = new DataStore(store);
    }

    @Override
    public void onVideoLessonListAvailable(List<VideoLesson> videoLessonList) {
        videoLessonListMediatorLiveData.setValue(videoLessonList);
    }

    @Override
    public LiveData<List<VideoLesson>> getVideoLessonListLiveData() {
        return videoLessonListMediatorLiveData;
    }

    @Override
    public void attachVideoLessonListListener() {
        firebaseDataStore.addOnVideoLessonListAvailableListener(this);
        firebaseDataStore.attachVideoLessonListListener();
    }
}