package pl.fundacjakult.edubike.features.lesson_list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.item_lesson_list.view.*
import org.jetbrains.anko.imageResource
import pl.fundacjakult.edubike.R
import pl.fundacjakult.edubike.domain.model.LessonModel
import pl.fundacjakult.edubike.framework.model.LessonFrameworkModel

class LessonListAdapter(private val onClickListener: View.OnClickListener) : RecyclerView.Adapter<ViewHolder>() {

    var lessonList = ArrayList<LessonModel>()

    private val drawableList: Array<Int> = arrayOf(
            R.drawable.karta_rowerowa,
            R.drawable.wyposazenie_i_budowa_roweru,
            R.drawable.znaki_drogowe,
            R.drawable.uczestnik_ruchu_drogowego,
            R.drawable.sygnalizacja_swietlna,
            R.drawable.pierwsza_pomoc,
            R.drawable.pierwszenstwo_przejazdu,
            R.drawable.przepisy_ogolne,
            R.drawable.kask_korzystanie_w_asciwosci,
            R.drawable.widocznosc_na_drodze)

    fun updateLessonList(list: ArrayList<LessonModel>) {
        this.lessonList = list
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_lesson_list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return LessonViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        (holder as LessonViewHolder).bindData(lessonList[position], drawableList[position])
    }

    override fun getItemCount(): Int {
        return lessonList.size
    }

    private inner class LessonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val labelTextView = itemView.text_lesson_subject
        private val labelIcon = itemView.image_lesson_icon
        private val labelCheck = itemView.image_lesson_status_icon

        fun bindData(lesson: LessonModel, drawable: Int) {
            labelTextView.text = lesson.name
            labelIcon.imageResource = drawable
            if (lesson.isCompleted) labelCheck.imageResource = R.drawable.ic_done_yellow
            itemView.setOnClickListener(this@LessonListAdapter.onClickListener)
        }
    }
}