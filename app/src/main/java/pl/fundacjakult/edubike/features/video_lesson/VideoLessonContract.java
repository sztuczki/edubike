package pl.fundacjakult.edubike.features.video_lesson;

import java.util.List;

import androidx.lifecycle.LiveData;

public interface VideoLessonContract {

    interface View {
    }

    interface Presenter {
        void attachVideoLessonListListener();

        LiveData<List<VideoLesson>> getVideoLessonListLiveData();
    }
}