package pl.fundacjakult.edubike.features.video_lesson;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.youtube.player.YouTubeStandalonePlayer;

import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import pl.fundacjakult.edubike.MainActivity;
import pl.fundacjakult.edubike.R;
import pl.fundacjakult.edubike.features.video_lesson.youtube_player.YouTubeConfig;
import pl.fundacjakult.edubike.navigation_utils.NavigationProvider;
import pl.fundacjakult.edubike.navigation_utils.Navigator;

public class VideoLessonListFragment extends Fragment {

    private VideoLessonListPresenter videoLessonListPresenter;

    private RecyclerView videoLessonListRecyclerView;
    private VideoLessonListAdapter videoLessonListAdapter;
    private Toolbar videoLessonListToolbar;

    public VideoLessonListFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupVideoLessonListAdapter();
        videoLessonListPresenter = new VideoLessonListPresenter();
        setupLessonListLiveData();
        setHasOptionsMenu(true);
    }

    @Override
    public void onStart() {
        videoLessonListPresenter.attachVideoLessonListListener();
        ((MainActivity) Objects.requireNonNull(getActivity())).getSupportActionBar().show();
        super.onStart();
    }

    private void setupToolbar() {
        if (getActivity() != null) {
            ((MainActivity) getActivity()).setSupportActionBar(videoLessonListToolbar);
            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setDisplayShowTitleEnabled(false);
            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setHomeAsUpIndicator(R.drawable.up_arrow);
            Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_video_lesson_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setupRecyclerView(view);
        videoLessonListToolbar = view.findViewById(R.id.video_lesson_list_toolbar);
        setupToolbar();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                if (getActivity() != null) getActivity().onBackPressed();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    private void setupVideoLessonListAdapter() {
        videoLessonListAdapter = new VideoLessonListAdapter(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onListItemClicked(view);
            }
        });
    }

    private void setupLessonListLiveData() {
        LiveData<List<VideoLesson>> videoLessonLiveData = videoLessonListPresenter.getVideoLessonListLiveData();
        videoLessonLiveData.observe(this, new Observer<List<VideoLesson>>() {
            @Override
            public void onChanged(@Nullable List<VideoLesson> videoLessonList) {
                videoLessonListAdapter.setVideoLessonList(videoLessonList);
            }
        });
    }

    private void onListItemClicked(View view) {
        int position = videoLessonListRecyclerView.getChildAdapterPosition(view);
        String videoId = videoLessonListAdapter.videoLessonList.get(position).videoId;
        if (getActivity() != null) {
            Intent intent = YouTubeStandalonePlayer.createVideoIntent(getActivity(), YouTubeConfig.getYouTubeApiKey(), videoId);
            startActivity(intent);
        }
    }

    private void setupRecyclerView(View view) {
        DividerItemDecoration itemDecoration = new DividerItemDecoration(Objects.requireNonNull(getContext()), DividerItemDecoration.VERTICAL);
        itemDecoration.setDrawable(Objects.requireNonNull(ContextCompat.getDrawable(getContext(), R.drawable.list_item_decoration)));
        videoLessonListRecyclerView = view.findViewById(R.id.video_lesson_picker_recycler_view);
        videoLessonListRecyclerView.addItemDecoration(itemDecoration);
        videoLessonListRecyclerView.setAdapter(videoLessonListAdapter);
        videoLessonListRecyclerView.setLayoutManager(new LinearLayoutManager(getContext().getApplicationContext()));
        videoLessonListAdapter.notifyDataSetChanged();
    }

    private Navigator getNavigator() {
        if (getActivity() instanceof NavigationProvider) {
            return ((MainActivity) getActivity()).getNavigator();
        }
        return null;
    }
}