package pl.fundacjakult.edubike.features.menu

import android.graphics.Bitmap
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import pl.fundacjakult.edubike.connectivity_utils.NetworkInfoProvider
import pl.fundacjakult.edubike.data_store.LocalDatabaseVersionManager
import pl.fundacjakult.edubike.data_store.OnAllImagesDownloadedListener
import pl.fundacjakult.edubike.data_store.OnImageDownloadedListener
import pl.fundacjakult.edubike.data_store.firebase.DataStoreProvider
import pl.fundacjakult.edubike.data_store.firebase.FirebaseDataDownloadListener
import pl.fundacjakult.edubike.data_store.firebase.OnDataAvailableListener
import pl.fundacjakult.edubike.framework.model.LessonContentPartFrameworkModel
import pl.fundacjakult.edubike.framework.model.LessonFrameworkModel
import pl.fundacjakult.edubike.framework.model.QuizQuestionFrameworkModel
import pl.fundacjakult.edubike.data_store.room.LocalDao
import pl.fundacjakult.edubike.data_store.room.OnDataSavedListener
import pl.fundacjakult.edubike.data_store.room.room_cleaner.OnRoomDataDeletedListener
import pl.fundacjakult.edubike.data_store.room.room_cleaner.RoomCleaner
import pl.fundacjakult.edubike.image_utils.ImageLoader
import pl.fundacjakult.edubike.image_utils.ImageSaver
import pl.fundacjakult.edubike.navigation_utils.Navigator
import pl.fundacjakult.edubike.navigation_utils.WebBrowserIntentProvider

class MenuPresenter(private val view: MenuContract.View,
                    private val localDatabaseVersionManager: LocalDatabaseVersionManager,
                    private val localDao: LocalDao,
                    private val imageLoader: ImageLoader,
                    private val imageSaver: ImageSaver,
                    private val navigator: Navigator,
                    private val browserIntentProvider: WebBrowserIntentProvider,
                    private val roomCleaner: RoomCleaner,
                    private val networkMonitor: NetworkInfoProvider,
                    private val dataStore: DataStoreProvider) : MenuContract.Presenter,
        OnDataAvailableListener, FirebaseDataDownloadListener, OnImageDownloadedListener,
        OnDataSavedListener, OnRoomDataDeletedListener, OnAllImagesDownloadedListener {

    private lateinit var onlineDatabaseVersion: String
    private var wasLessonListSaved = false
    private var wasContentPartListSaved = false
    private var wasQuizQuestionListSaved = false
    private var wereAllImagesSaved = false
    private var wasDownloadSuccessful = true

    init {
        imageLoader.firebaseDataDownloadListener = this
        imageLoader.onImageDownloadedListener = this
        imageLoader.onAllImagesDownloadedListener = this
        imageSaver.firebaseDataDownloadListener = this
        roomCleaner.onRoomDataDeletedListener = this
    }

    fun checkForDatabaseUpdates() {
        if (networkMonitor.isConnected()) {
            dataStore.addOnDataAvailableListener(this)
            dataStore.attachVersionListener()
        } else {
            if (localDatabaseVersionManager.hasLocalDatabase()) {
                if (localDatabaseVersionManager.getLocalDatabaseVersion() == "Connection Required"
                        || localDatabaseVersionManager.getLocalDatabaseVersion() == "Downloading Error"
                        || localDatabaseVersionManager.getLocalDatabaseVersion() == "Deleted") {
                    onConnectionRequired()
                } else {
                    view.shouldShowSplash(false)
                }
            } else {
                onConnectionRequired()
            }
        }
    }

    override fun onDatabaseVersionAvailable(databaseVersion: String) {
        onlineDatabaseVersion = databaseVersion
        if (localDatabaseVersionManager.hasLocalDatabase()) {
            if (localDatabaseVersionManager.getLocalDatabaseVersion() == databaseVersion) {
                view.hideSplashScreen()
            } else {
                roomCleaner.clearLocalDatabase()
            }
        } else {
            roomCleaner.clearLocalDatabase()
        }
    }

    private fun attachLessonListListener() {
        onFirebaseDataDownloadingStarted()
        dataStore.attachLessonListListener()
    }

    private fun attachContentPartListListener() {
        dataStore.attachContentPartListListener()
    }

    private fun attachQuizQuestionListListener() {
        dataStore.attachQuizQuestionListListener()
    }

    override fun onLessonListAvailable(lessonList: List<LessonFrameworkModel>) {
        saveDataStoreLessonList(lessonList)
        attachContentPartListListener()
        attachQuizQuestionListListener()
    }

    override fun onContentPartListAvailable(contentPartList: List<LessonContentPartFrameworkModel>) {
        for (contentPart in contentPartList) {
            if (contentPart.filename != null) {
                imageLoader.imagesToLoad = imageLoader.imagesToLoad.inc()
                imageLoader.loadImage(contentPart.data, contentPart.filename!!)
            }
        }
        saveDataStoreContentPartList(contentPartList)
    }

    override fun onQuizQuestionListAvailable(quizQuestionList: List<QuizQuestionFrameworkModel>) {
        for (quizQuestion in quizQuestionList) {
            if (quizQuestion.imageUrl != null) {
                imageLoader.imagesToLoad = imageLoader.imagesToLoad.inc()
                imageLoader.loadImage(quizQuestion.imageUrl!!, quizQuestion.filename!!)
            }
        }
        saveDataStoreQuizQuestionList(quizQuestionList)
    }

    override fun onFirebaseDataDownloadingStarted() {
        view.updateSplashMessage("Pobieranie lekcji - może to potrwać kilka minut")
    }

    override fun onImageDownloaded(bitmap: Bitmap?, filename: String) {
        imageSaver.saveImageToFile(bitmap, filename)
    }

    override fun onAllImagesDownloaded() {
        wereAllImagesSaved = true
        wasAllFirebaseDataSaved()
    }

    override fun onFirebaseDataDownloaded() {
        view.hideSplashScreen()
    }

    private fun saveDataStoreLessonList(lessonList: List<LessonFrameworkModel>) {
        doAsync {
            localDao.insertAll(lessonList)
            uiThread {
                onLessonListSaved()
            }
        }
    }

    private fun saveDataStoreContentPartList(contentPartList: List<LessonContentPartFrameworkModel>) {
        doAsync {
            localDao.insertLessonContentParts(contentPartList)
            uiThread {
                onContentPartListSaved()
            }
        }
    }

    private fun saveDataStoreQuizQuestionList(quizQuestionList: List<QuizQuestionFrameworkModel>) {
        doAsync {
            localDao.insertLessonQuizzes(quizQuestionList)
            uiThread {
                onQuizQuestionListSaved()
            }
        }
    }

    override fun onLessonListSaved() {
        wasLessonListSaved = true
        wasAllFirebaseDataSaved()
    }

    override fun onContentPartListSaved() {
        wasContentPartListSaved = true
        wasAllFirebaseDataSaved()
    }

    override fun onQuizQuestionListSaved() {
        wasQuizQuestionListSaved = true
        wasAllFirebaseDataSaved()
    }

    private fun wasAllFirebaseDataSaved() {
        if (wasDownloadSuccessful) {
            if (wasLessonListSaved && wasContentPartListSaved && wasQuizQuestionListSaved && wereAllImagesSaved) {
                onFirebaseDataDownloaded()
                localDatabaseVersionManager.onLocalDataSuccessfullySaved(onlineDatabaseVersion)
            }
        } else {
            localDatabaseVersionManager.onDataDownloadingFailed()
        }
    }

    override fun onRoomDataDeleted() {
        localDatabaseVersionManager.onLocalDataDeleted()
        attachLessonListListener()
    }

    override fun onConnectionRequired() {
        wasDownloadSuccessful = false
        localDatabaseVersionManager.onConnectionRequired()
        view.showConnectionRequiredDialog()
    }

    override fun onFirebaseDataDownloadingFailed() {
        wasDownloadSuccessful = false
        localDatabaseVersionManager.onDataDownloadingFailed()
        view.showConnectionInterruptedDialog()
    }

    override fun navigateToLessonListFragment() {
        navigator.navigateToLessonListFragment()
    }

    override fun navigateToVideoLessonListFragment() {
        navigator.navigateToVideoLessonListFragment()
    }

    override fun navigateToPartnersFragment() {
        navigator.navigateToPartnersFragment()
    }

    override fun navigateToEventsFragment() {
        browserIntentProvider.launchFacebookEventsIntent()
    }

    override fun navigateToPrivacyPolicyWebsite() {
        browserIntentProvider.launchPrivacyPolicyWebsiteIntent()
    }
}