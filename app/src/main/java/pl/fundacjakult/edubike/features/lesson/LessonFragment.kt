package pl.fundacjakult.edubike.features.lesson

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_lesson.*
import pl.fundacjakult.edubike.R
import pl.fundacjakult.edubike.data.DataRepository
import pl.fundacjakult.edubike.data_store.room.RoomDataStore
import pl.fundacjakult.edubike.data_store.room.RoomDatabaseProvider
import pl.fundacjakult.edubike.data_store.room.RxLocalDao
import pl.fundacjakult.edubike.domain.model.LessonContentPartModel
import pl.fundacjakult.edubike.domain.model.LessonModel
import pl.fundacjakult.edubike.image_utils.ImageProvider
import pl.fundacjakult.edubike.navigation_utils.NavigationProvider
import pl.fundacjakult.edubike.navigation_utils.Navigator
import pl.fundacjakult.edubike.usecases.GetContentPartsForLesson
import pl.fundacjakult.edubike.views.ItemOffsetDecoration

class LessonFragment : Fragment(), LessonContract.View {

    private lateinit var lessonId: String
    private lateinit var presenter: LessonContract.Presenter
    private lateinit var adapter: ContentPartAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState != null) lessonId = savedInstanceState.getString("lessonId")!!
        val imageProvider = ImageProvider(context!!.applicationContext)
        adapter = ContentPartAdapter(imageProvider)
        val roomDataStore: DataRepository.DataPersistentSource = RoomDataStore(getRxLocalDao())
        val localRepository = DataRepository(roomDataStore)
        val getContentPartsForLesson = GetContentPartsForLesson(localRepository)
        presenter = LessonPresenter(this, getContentPartsForLesson, getNavigator())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_lesson, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val itemDecoration = ItemOffsetDecoration(context!!, R.dimen.content_view_offset)
        rv_content_parts?.apply {
            layoutManager = LinearLayoutManager(context)
            this.adapter = this@LessonFragment.adapter
            addItemDecoration(itemDecoration)
        }
//        .layoutManager = LinearLayoutManager(context)
//        rv_content_parts?.adapter = adapter
//        rv_content_parts?.addItemDecoration(itemDecoration)
        start_quiz_button?.setOnClickListener { presenter.navigateToQuizFragment(lessonId) }
    }

    override fun onResume() {
        super.onResume()
        presenter.observeLessonContents(lessonId)
    }

    override fun updateFor(contentParts: List<LessonContentPartModel>) {
        adapter.updateContentPartList(contentParts)
    }

    fun setLesson(lesson: LessonModel) {
        this.lessonId = lesson.lessonId
    }

    private fun getNavigator(): Navigator {
        return (activity as NavigationProvider).getNavigator()
    }

    private fun getRxLocalDao(): RxLocalDao {
        return (activity?.application as RoomDatabaseProvider).getRxLocalDao()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("lessonId", lessonId)
        super.onSaveInstanceState(outState)
    }
}