package pl.fundacjakult.edubike.features.quiz

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.button_quiz.view.*
import kotlinx.android.synthetic.main.fragment_quiz.*
import kotlinx.android.synthetic.main.quiz_summary_layout.view.*
import org.jetbrains.anko.textColor
import pl.fundacjakult.edubike.R
import pl.fundacjakult.edubike.framework.model.QuizQuestionFrameworkModel
import pl.fundacjakult.edubike.data_store.room.LocalDao
import pl.fundacjakult.edubike.data_store.room.RoomDatabaseProvider
import pl.fundacjakult.edubike.data_store.room.RxLocalDao
import pl.fundacjakult.edubike.image_utils.ImageProvider
import pl.fundacjakult.edubike.navigation_utils.NavigationProvider
import pl.fundacjakult.edubike.navigation_utils.Navigator
import java.util.*

class QuizFragment : Fragment(), QuizContract.View {

    private var lessonId: String? = null
    private lateinit var presenter: QuizPresenter
    private lateinit var imageProvider: ImageProvider
    private lateinit var answerButtonList: ArrayList<View>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState != null) lessonId = savedInstanceState.getString("lessonId")
        presenter = QuizPresenter(getRxLocalDao(), getLocalDao(), lessonId, getNavigator(), this, QuizRoulette())
        imageProvider = ImageProvider(context?.applicationContext)
    }

    override fun onResume() {
        super.onResume()
        val activeQuestionData = presenter.getActiveQuestionData()
        activeQuestionData.observe(this, Observer { quizQuestion ->
            updateForQuestion(quizQuestion)
        })
    }

    override fun getLifecycleOwner(): LifecycleOwner {
        return this
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_quiz, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        addButtonAnswerTags()
        updateAnswerButtonList()
        attachNavigationButtonsClickListeners()
    }

    override fun showQuizSummary(score: Int) {
        disableNextQuestionButton()
        disablePreviousQuestionButton()
        val summaryView = quiz_summary_view_stub?.inflate()
        summaryView?.text_score?.text = "Twój wynik to: $score"
        if (presenter.wasPassed()) {
            if (lessonId != null) presenter.saveResult(lessonId!!)
            summaryView?.text_summary?.text = resources.getString(R.string.summary_lesson_completed)
        } else {
            summaryView?.text_summary?.text = resources.getString(R.string.summary_lesson_failed)
        }
        summaryView?.button_finish?.setOnClickListener { presenter.returnToLessonListFragment() }
    }

    override fun updateForQuestion(question: QuizQuestionFrameworkModel) {
        resetAnswerButtons()
        quiz_text_view?.text = question.question
        button_answer_a?.button_quiz_text?.text = question.optionA
        button_answer_b?.button_quiz_text?.text = question.optionB
        button_answer_c?.button_quiz_text?.text = question.optionC
        button_answer_d?.button_quiz_text?.text = question.optionD
        if (question.filename != null) {
            imageProvider.loadInto(quiz_image_view, question.filename)
            quiz_image_view?.visibility = View.VISIBLE
        } else {
            quiz_image_view?.visibility = View.GONE
        }
        if (question.chosenAnswer == null) {
            attachAnswerButtonClickListeners()
        } else {
            detachAnswerButtonClickListeners()
            updateAnswerBackgrounds(question.answer, question.chosenAnswer!!)
        }
        setupNavigationButtons()
    }

    private fun resetAnswerButtons() {
        val colorStateList = ColorStateList.valueOf(ResourcesCompat.getColor(resources, R.color.item_background, null))
        for (button in answerButtonList) {
            button.button_quiz_tag.backgroundTintList = colorStateList
            button.button_quiz_text.background = resources.getDrawable(R.drawable.quiz_answer_background, null)
        }
    }

    private fun updateAnswerBackgrounds(answer: String, optionChosen: String) {
        val colorStateListRight = ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.colorAnswerRight))
        val colorStateListWrong = ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.colorAnswerWrong))
        if (answer == optionChosen) {
            (getButtonByTag(answer))?.button_quiz_text?.background = resources.getDrawable(R.drawable.quiz_answer_background_right, null)
            (getButtonByTag(answer))?.button_quiz_tag?.backgroundTintList = colorStateListRight
        } else {
            (getButtonByTag(answer))?.button_quiz_text?.background = resources.getDrawable(R.drawable.quiz_answer_background_right, null)
            (getButtonByTag(answer))?.button_quiz_tag?.backgroundTintList = colorStateListRight
            (getButtonByTag(optionChosen))?.button_quiz_text?.background = resources.getDrawable(R.drawable.quiz_answer_background_wrong, null)
            (getButtonByTag(optionChosen))?.button_quiz_tag?.backgroundTintList = colorStateListWrong
        }
    }

    private fun getButtonByTag(answer: String): View? {
        for (button in answerButtonList) {
            if (button.tag.toString() == answer) {
                return button
            }
        }
        return null
    }

    private fun addButtonAnswerTags() {
        button_answer_a?.tag = "A"
        button_answer_b?.tag = "B"
        button_answer_c?.tag = "C"
        button_answer_d?.tag = "D"
        button_answer_a?.button_quiz_tag?.text = "A"
        button_answer_b?.button_quiz_tag?.text = "B"
        button_answer_c?.button_quiz_tag?.text = "C"
        button_answer_d?.button_quiz_tag?.text = "D"
    }

    private fun updateAnswerButtonList() {
        answerButtonList = ArrayList()
        answerButtonList.add(button_answer_a)
        answerButtonList.add(button_answer_b)
        answerButtonList.add(button_answer_c)
        answerButtonList.add(button_answer_d)
    }

    private fun attachAnswerButtonClickListeners() {
        for (button in answerButtonList) {
            button.setOnClickListener { view ->
                val viewTag = view?.tag.toString()
                presenter.submitAnswer(viewTag)
            }
        }
    }

    private fun detachAnswerButtonClickListeners() {
        for (button in answerButtonList) {
            button.setOnClickListener(null)
        }
    }

    private fun attachNavigationButtonsClickListeners() {
        next_question_button.setOnClickListener { presenter.onNextQuestionClicked() }
        previous_question_button.setOnClickListener { presenter.onPreviousQuestionClicked() }
    }

    private fun setupNavigationButtons() {
        if (presenter.shouldEnableNextQuestionButton()) {
            enableNextQuestionButton()
        } else {
            disableNextQuestionButton()
        }
        if (presenter.shouldEnablePrevQuestionButton()) {
            enablePreviousQuestionButton()
        } else {
            disablePreviousQuestionButton()
        }
    }

    private fun enableNextQuestionButton() {
        next_question_button?.isEnabled = true
        next_question_button?.textColor = ResourcesCompat.getColor(resources, R.color.colorWhite, null)
    }

    private fun disableNextQuestionButton() {
        next_question_button?.isEnabled = false
        next_question_button?.textColor = ResourcesCompat.getColor(resources, R.color.quiz_nav_button_text_disabled, null)
    }

    private fun enablePreviousQuestionButton() {
        previous_question_button?.isEnabled = true
        previous_question_button?.textColor = ResourcesCompat.getColor(resources, R.color.colorWhite, null)
    }

    private fun disablePreviousQuestionButton() {
        previous_question_button?.isEnabled = false
        previous_question_button?.textColor = ResourcesCompat.getColor(resources, R.color.quiz_nav_button_text_disabled, null)
    }

    fun setLesson(lessonId: String) {
        this.lessonId = lessonId
    }

    private fun getRxLocalDao(): RxLocalDao {
        return (activity?.application as RoomDatabaseProvider).getRxLocalDao()
    }

    private fun getLocalDao(): LocalDao {
        return (activity?.application as RoomDatabaseProvider).getLocalDao()
    }

    private fun getNavigator(): Navigator {
        return (activity as NavigationProvider).getNavigator()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        if (lessonId != null) outState.putString("lessonId", lessonId)
        super.onSaveInstanceState(outState)
    }
}