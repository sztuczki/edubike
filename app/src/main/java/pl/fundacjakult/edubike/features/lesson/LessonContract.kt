package pl.fundacjakult.edubike.features.lesson

import pl.fundacjakult.edubike.domain.model.LessonContentPartModel

interface LessonContract {

    interface View {
        fun updateFor(contentParts: List<LessonContentPartModel>)
    }

    interface Presenter {
        fun navigateToQuizFragment(lessonId: String)
        fun observeLessonContents(lessonId: String)
        fun onStop()
        fun onDestroy()
    }
}