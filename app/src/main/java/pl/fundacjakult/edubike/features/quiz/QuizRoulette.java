package pl.fundacjakult.edubike.features.quiz;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import pl.fundacjakult.edubike.framework.model.QuizQuestionFrameworkModel;

public class QuizRoulette {

    private final List<List<QuizQuestionFrameworkModel>> questionPool = new ArrayList<>();

    void addQuestionsForLesson(List<QuizQuestionFrameworkModel> questions) {
        questionPool.add(questions);
    }

    ArrayList<QuizQuestionFrameworkModel> pickQuizQuestions(int questionsPerLesson) {
        ArrayList<QuizQuestionFrameworkModel> pickedQuizQuestions = new ArrayList<>();
        Random random = new Random();
        for (List<QuizQuestionFrameworkModel> lessonQuestions : questionPool) {
            if (lessonQuestions.size() < questionsPerLesson) {
                throw new IndexOutOfBoundsException();
            }
            for (int i = 0; i < questionsPerLesson; i++) {
                QuizQuestionFrameworkModel quizQuestion = lessonQuestions.get(random.nextInt(lessonQuestions.size()));
                if (pickedQuizQuestions.contains(quizQuestion)) {
                    if (i != 0) {
                        i--;
                    }
                } else {
                    pickedQuizQuestions.add(quizQuestion);
                }
            }
        }
        return pickedQuizQuestions;
    }
}