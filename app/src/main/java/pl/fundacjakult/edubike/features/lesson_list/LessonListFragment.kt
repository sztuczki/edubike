package pl.fundacjakult.edubike.features.lesson_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_lesson_list.*
import pl.fundacjakult.edubike.KultApplication
import pl.fundacjakult.edubike.MainActivity
import pl.fundacjakult.edubike.R
import pl.fundacjakult.edubike.domain.model.LessonModel
import pl.fundacjakult.edubike.navigation_utils.Navigator
import pl.fundacjakult.edubike.usecases.GetLessonListWithStatus
import javax.inject.Inject

class LessonListFragment : Fragment(), LessonListContract.View, View.OnClickListener {

    private lateinit var presenter: LessonListContract.Presenter
    private var adapter: LessonListAdapter? = null
    private var lessonList = ArrayList<LessonModel>()

    lateinit var navigator: Navigator

    @Inject
    lateinit var getLessonListWithStatus: GetLessonListWithStatus

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        navigator = MainActivity.mainActivityComponent!!.getNavigator()
        KultApplication.component?.inject(this)
        presenter = LessonListPresenter(this, navigator, getLessonListWithStatus)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_lesson_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupToolbar()
        setupRecyclerView()
    }

    override fun onStart() {
        super.onStart()
        presenter.observeLessonData()
    }

    override fun onResume() {
        super.onResume()
        shouldShowBigQuizButton()
    }

    private fun setupToolbar() {
        (activity as MainActivity).setSupportActionBar(lesson_list_toolbar as Toolbar)
        (activity as MainActivity).supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as MainActivity).supportActionBar?.setHomeAsUpIndicator(R.drawable.up_arrow)
        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setupRecyclerView() {
        rv_lesson_list?.layoutManager = LinearLayoutManager(context)
        rv_lesson_list?.adapter = getLessonListAdapter()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> activity?.onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getLessonListAdapter(): LessonListAdapter {
        return adapter ?: LessonListAdapter(this).also { adapter = it }
    }

    override fun onClick(v: View) {
        val position = rv_lesson_list.getChildAdapterPosition(v)
        val lesson = getLessonListAdapter().lessonList[position]
        presenter.navigateToLessonFragment(lesson)
    }

    override fun onLessonDataUpdated(lessonList: List<LessonModel>) {
        this.lessonList = lessonList as ArrayList<LessonModel>
        getLessonListAdapter().updateLessonList(lessonList)
        shouldShowBigQuizButton()
    }

    private fun shouldShowBigQuizButton() {
        for (lesson in lessonList) {
            if (!lesson.isCompleted) {
                return
            }
            if (lessonList[lessonList.size - 1] == lesson) {
                big_quiz_button?.visibility = View.VISIBLE
                big_quiz_button?.setOnClickListener { presenter.navigateToBigQuizFragment() }
            }
        }
    }

    override fun onStop() {
        adapter = null
        presenter.onStop()
        super.onStop()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }
}