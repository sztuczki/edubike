package pl.fundacjakult.edubike.features.quiz

import androidx.lifecycle.LifecycleOwner
import pl.fundacjakult.edubike.framework.model.QuizQuestionFrameworkModel

interface QuizContract {

    interface View {
        fun getLifecycleOwner(): LifecycleOwner
        fun updateForQuestion(question: QuizQuestionFrameworkModel)
        fun showQuizSummary(score: Int)
    }

    interface Presenter
}