package pl.fundacjakult.edubike.features.video_lesson;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import pl.fundacjakult.edubike.R;
import pl.fundacjakult.edubike.features.video_lesson.youtube_player.YouTubeConfig;

public class VideoLessonListAdapter extends RecyclerView.Adapter<VideoLessonListAdapter.ListItemViewHolder> {

    public List<VideoLesson> videoLessonList;
    private View.OnClickListener onClickListener;

    VideoLessonListAdapter(View.OnClickListener onClickListener) {
        videoLessonList = new ArrayList<>();
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public ListItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext().getApplicationContext()).inflate(viewType, parent, false);
        return new ListItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListItemViewHolder holder, int position) {
        holder.bindData(videoLessonList.get(position));
    }

    @Override
    public int getItemCount() {
        return videoLessonList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.video_lesson_list_item;
    }

    class ListItemViewHolder extends RecyclerView.ViewHolder {

        View itemView;
        TextView lessonNameTextView;
        YouTubeThumbnailView youTubeThumbnailViewReference;

        ListItemViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            lessonNameTextView = itemView.findViewById(R.id.list_item_view);
            youTubeThumbnailViewReference = itemView.findViewById(R.id.youtube_thumbnail_view);
        }

        void bindData(final VideoLesson videoLesson) {
            lessonNameTextView.setText(videoLesson.name);
            itemView.setOnClickListener(onClickListener);
            youTubeThumbnailViewReference.initialize(YouTubeConfig.getYouTubeApiKey(), new YouTubeThumbnailView.OnInitializedListener() {
                @Override
                public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, final YouTubeThumbnailLoader youTubeThumbnailLoader) {
                    youTubeThumbnailLoader.setVideo(videoLesson.videoId);
                    youTubeThumbnailLoader.setOnThumbnailLoadedListener(new YouTubeThumbnailLoader.OnThumbnailLoadedListener() {
                        @Override
                        public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView, String s) {
                            youTubeThumbnailLoader.release();
                        }

                        @Override
                        public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader.ErrorReason errorReason) {
                        }
                    });
                }

                @Override
                public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {

                }
            });
        }
    }

    void setVideoLessonList(List<VideoLesson> videoLessonList) {
        this.videoLessonList = videoLessonList;
        notifyDataSetChanged();
    }
}