package pl.fundacjakult.edubike.connectivity_utils

interface NetworkInfoProvider {

    fun isConnected(): Boolean
}