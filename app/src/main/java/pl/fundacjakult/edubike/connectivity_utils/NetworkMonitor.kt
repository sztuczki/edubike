package pl.fundacjakult.edubike.connectivity_utils

import android.net.ConnectivityManager

class NetworkMonitor(private val connectivityManager: ConnectivityManager) : NetworkInfoProvider {

    override fun isConnected(): Boolean {
        val activeNetwork = connectivityManager.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnected
    }
}