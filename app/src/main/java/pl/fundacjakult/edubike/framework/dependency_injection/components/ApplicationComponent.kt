package pl.fundacjakult.edubike.framework.dependency_injection.components

import dagger.Component
import pl.fundacjakult.edubike.features.lesson_list.LessonListFragment
import pl.fundacjakult.edubike.framework.dependency_injection.modules.KultApplicationModule
import pl.fundacjakult.edubike.framework.dependency_injection.modules.RepositoryModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    KultApplicationModule::class,
    RepositoryModule::class])
interface ApplicationComponent {

    fun inject(repositoryModule: RepositoryModule)
    fun inject(lessonListFragment: LessonListFragment)
}