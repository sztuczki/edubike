package pl.fundacjakult.edubike.framework.dependency_injection.modules

import dagger.Module
import dagger.Provides
import pl.fundacjakult.edubike.data_store.room.LocalDao
import pl.fundacjakult.edubike.data_store.room.RoomDataStore
import pl.fundacjakult.edubike.data_store.room.RxLocalDao
import javax.inject.Singleton

@Module
class KultApplicationModule(private val localRxDao: RxLocalDao,
                            private val localDao: LocalDao) {

    @Provides
    fun localRxDao(): RxLocalDao {
        return localRxDao
    }

    @Provides
    fun localDao(): LocalDao {
        return localDao
    }
}