package pl.fundacjakult.edubike.framework.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import pl.fundacjakult.edubike.domain.model.LessonContentPartModel;

@Entity(indices = {@Index("lesson")},
        tableName = "content_part_list",
        foreignKeys = @ForeignKey(
                entity = LessonFrameworkModel.class,
                parentColumns = "lesson_id",
                childColumns = "lesson"))

public class LessonContentPartFrameworkModel implements LessonContentPartModel {

    @PrimaryKey(autoGenerate = true)
    public long id;
    public String lesson;
    private String data;
    private String type;
    @Nullable
    private String filename;
    @Nullable
    private String caption;

    @Override
    public long getId() {
        return id;
    }

    @NonNull
    public String getLesson() {
        return lesson;
    }

    @NonNull
    public String getData() {
        return data;
    }

    @NonNull
    public String getType() {
        return type;
    }

    @Nullable
    public String getFilename() {
        return filename;
    }

    @Nullable
    public String getCaption() {
        return caption;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setFilename(@Nullable String filename) {
        this.filename = filename;
    }

    public void setCaption(@Nullable String caption) {
        this.caption = caption;
    }
}