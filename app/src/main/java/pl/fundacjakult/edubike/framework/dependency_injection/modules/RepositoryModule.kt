package pl.fundacjakult.edubike.framework.dependency_injection.modules

import dagger.Module
import dagger.Provides
import pl.fundacjakult.edubike.data.DataRepository.DataPersistentSource
import pl.fundacjakult.edubike.data_store.room.RoomDataStore
import javax.inject.Inject

@Module
class RepositoryModule @Inject constructor(private val roomDataStore: RoomDataStore) {

    @Provides
    fun getPersistentSource(): DataPersistentSource {
        return roomDataStore
    }
}