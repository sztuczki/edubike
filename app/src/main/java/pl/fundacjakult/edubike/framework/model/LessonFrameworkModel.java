package pl.fundacjakult.edubike.framework.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.firebase.database.PropertyName;

import pl.fundacjakult.edubike.domain.model.LessonModel;

@Entity(tableName = "lesson_list")
public class LessonFrameworkModel implements LessonModel {

    @PrimaryKey
    @ColumnInfo(name = "lesson_id")
    @PropertyName("lesson_id")
    @NonNull
    private String lessonId = "";
    private String name;
    private String category;
    @Ignore
    private boolean isCompleted = false;

    public LessonFrameworkModel() {
    }

    @Ignore
    public LessonFrameworkModel(@NonNull String lessonId, String name, String category) {
        setName(name);
        setCategory(category);
        this.lessonId = lessonId;
        this.category = category;
    }

    @PropertyName("lesson_id")
    @NonNull
    @Override
    public String getLessonId() {
        return lessonId;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public String getCategory() {
        return category;
    }

    @Override
    public boolean isCompleted() {
        return isCompleted;
    }

    public void setLessonId(@NonNull String lessonId) {
        this.lessonId = lessonId;
    }

    public void setName(String lessonName) {
        this.name = lessonName;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }
}