package pl.fundacjakult.edubike.framework.dependency_injection.components

import dagger.Component
import pl.fundacjakult.edubike.features.lesson_list.LessonListFragment
import pl.fundacjakult.edubike.framework.dependency_injection.modules.ActivityModule
import pl.fundacjakult.edubike.framework.dependency_injection.modules.KultApplicationModule
import pl.fundacjakult.edubike.framework.dependency_injection.modules.RepositoryModule
import pl.fundacjakult.edubike.navigation_utils.Navigator

@Component(modules = [
    ActivityModule::class,
    KultApplicationModule::class])
interface MainActivityComponent {

//    fun inject(fragment: LessonListFragment)
    fun getNavigator(): Navigator
//    fun inject(repositoryModule: RepositoryModule)
}