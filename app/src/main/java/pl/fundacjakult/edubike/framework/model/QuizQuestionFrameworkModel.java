package pl.fundacjakult.edubike.framework.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.firebase.database.PropertyName;

import org.jetbrains.annotations.NotNull;

import pl.fundacjakult.edubike.domain.model.QuizQuestionModel;

@Entity(indices = {@Index("lesson")},
        tableName = "quiz_question_list",
        foreignKeys = @ForeignKey(entity = LessonFrameworkModel.class,
                parentColumns = "lesson_id",
                childColumns = "lesson"))

public class QuizQuestionFrameworkModel implements QuizQuestionModel {

    @PrimaryKey(autoGenerate = true)
    public long id;
    @ColumnInfo(name = "lesson")
    private String lesson;
    private String question;
    private String answer;
    @PropertyName("A")
    private String optionA;
    @PropertyName("B")
    private String optionB;
    @PropertyName("C")
    private String optionC;
    @PropertyName("D")
    private String optionD;
    @Nullable
    private String imageUrl;
    @Nullable
    private String filename;
    @Ignore
    @Nullable
    private String chosenAnswer;

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @NotNull
    @Override
    public String getLesson() {
        return lesson;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }

    @NotNull
    @Override
    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    @NotNull
    @Override
    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @NotNull
    @PropertyName("A")
    @Override
    public String getOptionA() {
        return optionA;
    }

    public void setOptionA(String optionA) {
        this.optionA = optionA;
    }

    @NonNull
    @PropertyName("B")
    public String getOptionB() {
        return optionB;
    }

    public void setOptionB(String optionB) {
        this.optionB = optionB;
    }

    @NonNull
    @PropertyName("C")
    public String getOptionC() {
        return optionC;
    }

    public void setOptionC(String optionC) {
        this.optionC = optionC;
    }

    @NonNull
    @PropertyName("D")
    public String getOptionD() {
        return optionD;
    }

    public void setOptionD(String optionD) {
        this.optionD = optionD;
    }

    @Nullable
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(@Nullable String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Nullable
    public String getFilename() {
        return filename;
    }

    public void setFilename(@Nullable String filename) {
        this.filename = filename;
    }

    @Nullable
    @Override
    public String getChosenAnswer() {
        return chosenAnswer;
    }

    public void setChosenAnswer(@Nullable String chosenAnswer) {
        this.chosenAnswer = chosenAnswer;
    }
}