package pl.fundacjakult.edubike.framework.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import pl.fundacjakult.edubike.domain.model.CompletedLessonModel;

@Entity(tableName = "solved_lesson_list",
        foreignKeys = @ForeignKey(entity = LessonFrameworkModel.class,
                parentColumns = "lesson_id",
                childColumns = "id"))

public class CompletedLessonFrameworkModel implements CompletedLessonModel {

    @NonNull
    @PrimaryKey
    private String id;


    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }
}