package pl.fundacjakult.edubike.framework.dependency_injection.modules

import androidx.fragment.app.FragmentManager
import dagger.Module
import dagger.Provides
import pl.fundacjakult.edubike.MainActivity

@Module
class ActivityModule(var activity: MainActivity) {

    @Provides
    fun getSupportFragmentManager(): FragmentManager {
        return activity.supportFragmentManager
    }
}