package pl.fundacjakult.edubike.features.menu

import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.internal.verification.Times
import pl.fundacjakult.edubike.connectivity_utils.NetworkInfoProvider
import pl.fundacjakult.edubike.image_utils.ImageLoader
import pl.fundacjakult.edubike.image_utils.ImageSaver
import pl.fundacjakult.edubike.data_store.LocalDatabaseVersionManager
import pl.fundacjakult.edubike.data_store.firebase.DataStoreProvider
import pl.fundacjakult.edubike.data_store.room.LocalDao
import pl.fundacjakult.edubike.data_store.room.room_cleaner.RoomCleaner
import pl.fundacjakult.edubike.navigation_utils.Navigator
import pl.fundacjakult.edubike.navigation_utils.WebBrowserIntentProvider

class MenuPresenterTest {

    lateinit var presenter: MenuPresenter

    @Mock
    lateinit var view: MenuContract.View

    @Mock
    lateinit var localDao: LocalDao

    @Mock
    lateinit var localDatabaseVersionManager: LocalDatabaseVersionManager

    @Mock
    lateinit var imageLoader: ImageLoader

    @Mock
    lateinit var imageSaver: ImageSaver

    @Mock
    lateinit var navigator: Navigator

    @Mock
    lateinit var browserIntentProvider: WebBrowserIntentProvider

    @Mock
    lateinit var roomCleaner: RoomCleaner

    @Mock
    lateinit var networkInfoProvider: NetworkInfoProvider

    @Mock
    lateinit var dataStore: DataStoreProvider

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        presenter = Mockito.spy(MenuPresenter(view,
                localDatabaseVersionManager,
                localDao,
                imageLoader,
                imageSaver,
                navigator,
                browserIntentProvider,
                roomCleaner,
                networkInfoProvider,
                dataStore
        ))
    }

    @Test
    fun shouldRegisterAsOnDataAvailableListener() {
        Mockito.`when`(networkInfoProvider.isConnected()).thenReturn(true)
        presenter.checkForDatabaseUpdates()
        verify(dataStore, Times(1)).addOnDataAvailableListener(presenter)
        verify(dataStore, Times(1)).attachVersionListener()
    }

    @Test
    fun shouldPreventShowingSplash() {
        Mockito.`when`(networkInfoProvider.isConnected()).thenReturn(false)
        Mockito.`when`(localDatabaseVersionManager.hasLocalDatabase()).thenReturn(true)
        Mockito.`when`(localDatabaseVersionManager.getLocalDatabaseVersion()).thenReturn("1.1.1")
        presenter.checkForDatabaseUpdates()
        verify(view).shouldShowSplash(false)

    }

    @Test
    fun shouldShowConnectionRequired() {
        Mockito.`when`(networkInfoProvider.isConnected()).thenReturn(false)
        Mockito.`when`(localDatabaseVersionManager.hasLocalDatabase()).thenReturn(true)
        Mockito.`when`(localDatabaseVersionManager.getLocalDatabaseVersion()).thenReturn("Connection Required")
        presenter.checkForDatabaseUpdates()
        verify(view).showConnectionRequiredDialog()
    }

    @Test
    fun shouldShowConnectionRequiredOnFirstLaunch() {
        Mockito.`when`(networkInfoProvider.isConnected()).thenReturn(false)
        Mockito.`when`(localDatabaseVersionManager.hasLocalDatabase()).thenReturn(false)

        presenter.checkForDatabaseUpdates()

        verify(view).showConnectionRequiredDialog()
    }


    @Test
    fun shouldUpdateDatabase() {
        Mockito.`when`(localDatabaseVersionManager.hasLocalDatabase()).thenReturn(true)
        Mockito.`when`(localDatabaseVersionManager.getLocalDatabaseVersion()).thenReturn("1.0.0")
        //then
        presenter.onDatabaseVersionAvailable("1.1.1")
        verify(roomCleaner).clearLocalDatabase()
    }

    @Test
    fun shouldStartDownloadingLessonsWhenDatabaseCleared() {
        presenter.onRoomDataDeleted()
        verify(presenter, Times(1)).onFirebaseDataDownloadingStarted()
        verify(dataStore, Times(1)).attachLessonListListener()
    }
}