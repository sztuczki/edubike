package pl.fundacjakult.edubike.features.quiz;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class QuizRouletteTest {

    QuizRoulette quizRoulette;

    //wywola sie przed kazdym testem z automatu
    @Before
    public void setup() {
        quizRoulette = new QuizRoulette();
    }

    @Rule
    public Timeout timeout = Timeout.seconds(3);

    @Test
    public void doesReturnAValidNumberOfQuestionsPerCategory() throws Exception {
        //given
        fillRouletteWithQuestions(quizRoulette, 3, 5);
        //when
        List<JavaQuizQuestion> pickedQuizQuestionList = quizRoulette.pickQuizQuestions(5);
        //then
        assertThat(pickedQuizQuestionList.size(), equalTo(3 * 5));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void doesThrowExceptionIfAskedForTooManyQuestions() throws Exception {
        //given
        fillRouletteWithQuestions(quizRoulette, 3, 5);
        //when
        quizRoulette.pickQuizQuestions(7);
    }

    @Ignore
    @Test
    public void checkAnything() throws Exception {
        fillRouletteWithQuestions(quizRoulette, 100, 10000);
        quizRoulette.pickQuizQuestions(9999);
    }

    private void fillRouletteWithQuestions(QuizRoulette quizRoulette, int numberOfLessons, int numberOfQuestionsPerLesson) {
        for (int i = 0; i < numberOfLessons; i++) {
            List<JavaQuizQuestion> quizQuestionList = new ArrayList<>();
            for (int j = 0; j < numberOfQuestionsPerLesson; j++) {
                quizQuestionList.add(new JavaQuizQuestion());
            }
            quizRoulette.addQuestionsForLesson(quizQuestionList);
        }
    }
}