package pl.fundacjakult.edubike.features.quiz

import org.junit.Before
import org.junit.Test
import pl.fundacjakult.edubike.framework.model.QuizQuestionFrameworkModel
import java.util.*

class NewQuizRouletteTest {

    private lateinit var quizRoulette: QuizRoulette

    @Before
    fun setup() {
        quizRoulette = QuizRoulette()
    }

    @Test(expected = IndexOutOfBoundsException::class)
    fun doesThrowExceptionForTooManyQuestions() {
        fillRouletteWithQuestions(quizRoulette, 3, 5)
        quizRoulette.pickQuizQuestions(6)
    }

    private fun fillRouletteWithQuestions(quizRoulette: QuizRoulette, numberOfLessons: Int, numberOfQuestionsPerLesson: Int) {
        for (i in 0 until numberOfLessons) {
            val quizQuestionList = ArrayList<QuizQuestionFrameworkModel>()
            for (j in 0 until numberOfQuestionsPerLesson) {
                quizQuestionList.add(QuizQuestionFrameworkModel())
            }
            quizRoulette.addQuestionsForLesson(quizQuestionList)
        }
    }
}