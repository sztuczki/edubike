package pl.fundacjakult.edubike.features.quiz

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import org.amshove.kluent.shouldEqual
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.invocation.InvocationOnMock
import org.mockito.stubbing.Answer
import pl.fundacjakult.edubike.data_store.room.LocalDao
import pl.fundacjakult.edubike.framework.model.JavaQuizQuestion
import pl.fundacjakult.edubike.navigation_utils.Navigator

class NewQuizPresenterTest {

    lateinit var presenter: QuizPresenter
    val localDao = mock(LocalDao::class.java)
    val navigator = mock(Navigator::class.java)
    val view = mock(QuizContract.View::class.java)
    val roulette = mock(QuizRoulette::class.java)

    @JvmField
    @Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Test
    fun setUp() {
        val list = ArrayList<JavaQuizQuestion>()
        val question = JavaQuizQuestion()
        list.add(question)
        val mutableLiveData = MutableLiveData<List<JavaQuizQuestion>>()
        Mockito.`when`(localDao.getLessonQuizzes(ArgumentMatchers.anyString())).thenAnswer(object : Answer<LiveData<List<JavaQuizQuestion>>> {
            override fun answer(invocation: InvocationOnMock?): LiveData<List<JavaQuizQuestion>> {
                return mutableLiveData
            }
        })
        Mockito.`when`(roulette.pickQuizQuestions(ArgumentMatchers.anyInt())).thenReturn(list)
        presenter = QuizPresenter(localDao, "A001", navigator, view, roulette)
        val quizQuestionData = presenter.getActiveQuestionData()

        quizQuestionData.observeForever {
            val size = list.size
        }

        mutableLiveData.postValue(list)
        quizQuestionData.value shouldEqual question
    }

    @Test
    fun shouldDisplayNextQuestion() {
        val list = ArrayList<JavaQuizQuestion>()
        val questionOne = JavaQuizQuestion()
        val questionTwo = JavaQuizQuestion()
        list.add(questionOne)
        list.add(questionTwo)

        val mutableLiveData = MutableLiveData<List<JavaQuizQuestion>>()
        Mockito.`when`(localDao.getLessonQuizzes(ArgumentMatchers.anyString())).thenAnswer(object : Answer<LiveData<List<JavaQuizQuestion>>> {
            override fun answer(invocation: InvocationOnMock?): LiveData<List<JavaQuizQuestion>> {
                return mutableLiveData
            }
        })
        Mockito.`when`(roulette.pickQuizQuestions(ArgumentMatchers.anyInt())).thenReturn(list)

        presenter = QuizPresenter(localDao, "A001", navigator, view, roulette)
        val activeQuestionData = presenter.getActiveQuestionData()
        activeQuestionData.observeForever({})
        mutableLiveData.postValue(list)
        presenter.onNextQuestionClicked()
        activeQuestionData.value shouldEqual questionTwo
    }


    @Test
    fun `should update for new list`() {

    }
}