//package pl.fundacjakult.edubike.features.quiz;
//
////import org.junit.Before;
////import org.junit.Rule;
////import org.junit.Test;
//import org.junit.Before;
//import org.junit.Rule;
//import org.junit.Test;
//import org.mockito.*;
//import org.mockito.Mockito;
//import org.mockito.MockitoAnnotations;
//import org.mockito.invocation.InvocationOnMock;
//import org.mockito.stubbing.Answer;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import androidx.annotation.Nullable;
//import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
//import androidx.lifecycle.LiveData;
//import androidx.lifecycle.MutableLiveData;
//import androidx.lifecycle.Observer;
//import pl.fundacjakult.edubike.data_store.room.LocalDao;
//import pl.fundacjakult.edubike.data_store.room.LocalDao;
//import pl.fundacjakult.edubike.data_store.model.JavaQuizQuestion;
//import pl.fundacjakult.edubike.navigation_utils.Navigator;
//
//import static org.mockito.ArgumentMatchers.anyString;
//import static org.mockito.Mockito.when;
//import pl.fundacjakult.edubike.fragment_utils.Navigator;
//
//import static org.mockito.Mockito.anyInt;
//import static org.mockito.Mockito.anyString;
//import static org.mockito.Mockito.mock;
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.when;
//
//public class QuizPresenterTest {
//
//    QuizPresenter quizPresenter;
//    int quizQuestionIndex = 0;
//
//    private static List<JavaQuizQuestion> randomQuizQuestionList;
//
//    @Mock
//    Navigator mockNavigator;
//    @Mock
//    LocalDao mockLocalDao;
//    @Mock
//    QuizSummaryView mockQuizSummaryView;
//    @Mock
//    QuizRoulette mockQuizRoulette;
//    @Mock
//    QuizQuestionManager mockQuizQuestionManager;
//
//    @Rule
//    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();
//
//    @Before
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        randomQuizQuestionList = new ArrayList<>();
//        quizPresenter = Mockito.spy(new QuizPresenter(mockNavigator, mockLocalDao, mockQuizSummaryView, mockQuizRoulette, mockQuizQuestionManager));
//    }
//
//    @Test
//    public void doesPostQuizQuestionOnFetch() throws Exception {
//        //given
//        final List<JavaQuizQuestion> quizQuestionList = new ArrayList<>();
//        JavaQuizQuestion quizQuestion = new JavaQuizQuestion();
//        quizQuestionList.add(quizQuestion);
//        final QuizQuestionChangeListener quizQuestionChangeListener = mock(QuizQuestionChangeListener.class);
//        final MutableLiveData<List<JavaQuizQuestion>> mutableLiveData = new MutableLiveData<>();
//        when(mockLocalDao.getLessonQuizzes(anyString())).thenAnswer(new Answer<LiveData<List<JavaQuizQuestion>>>() {
//            @Override
//            public LiveData<List<JavaQuizQuestion>> answer(InvocationOnMock invocation) throws Throwable {
//                mutableLiveData.postValue(quizQuestionList);
//                return mutableLiveData;
//            }
//        });
//        when(mockQuizRoulette.pickQuizQuestions(anyInt())).thenReturn(quizQuestionList);
//
//        final LiveData<JavaQuizQuestion> liveDataQuizQestion = quizPresenter.getQuizQuestionLiveData();
//        liveDataQuizQestion.observeForever(new Observer<JavaQuizQuestion>() {
//            @Override
//            public void onChanged(@Nullable JavaQuizQuestion quizQuestion) {
//                quizQuestionChangeListener.onQuizQuestionChange(quizQuestion);
//            }
//        });
//
//        //when
//        quizPresenter.fetchFirstQuizQuestionForLesson(anyString());
//        //then
//        verify(quizQuestionChangeListener).onQuizQuestionChange(quizQuestion);
//        //TODO: w getQuizQuestionLiveData ma być jedna linijka
//    }
//
//    @Test
//    public void doesProperlyUpdateQuestionOnNextClicked() throws Exception {
//        //given
////        final List<JavaQuizQuestion> randomQuizQuestionList = new ArrayList<>();
//        populateRandomQuizQuestionList();
//        final QuizQuestionChangeListener quizQuestionChangeListener = mock(QuizQuestionChangeListener.class);
//        final MutableLiveData<List<JavaQuizQuestion>> mutableLiveData = new MutableLiveData<>();
//        when(mockLocalDao.getLessonQuizzes(anyString())).thenAnswer(new Answer<LiveData<List<JavaQuizQuestion>>>() {
//            @Override
//            public LiveData<List<JavaQuizQuestion>> answer(InvocationOnMock invocation) throws Throwable {
//                mutableLiveData.postValue(randomQuizQuestionList);
//                return mutableLiveData;
//            }
//        });
//
//        final LiveData<JavaQuizQuestion> liveDataQuizQuestion = quizPresenter.getQuizQuestionLiveData();
//        liveDataQuizQuestion.observeForever(new Observer<JavaQuizQuestion>() {
//            @Override
//            public void onChanged(@Nullable JavaQuizQuestion quizQuestion) {
//                quizQuestionChangeListener.onQuizQuestionChange(quizQuestion);
//            }
//        });
//        //when
//        when(quizPresenter.getRandomQuizQuestionList()).thenReturn(randomQuizQuestionList);
//        when(mockQuizQuestionManager.hasNextQuestion(0, 5)).thenReturn(true);
//        quizPresenter.onNextQuestionClicked();
//        verify(quizPresenter).updateValue();
//    }
//
//    private void populateQuizQuestionList(List<JavaQuizQuestion> quizQuestionList) {
//        for (int i = 0; i < 5; i++) {
//            JavaQuizQuestion quizQuestion = new JavaQuizQuestion();
//            quizQuestionList.add(quizQuestion);
//        }
//    }
//
//    private void populateRandomQuizQuestionList() {
//        for (int i = 0; i < 5; i++) {
//            JavaQuizQuestion quizQuestion = new JavaQuizQuestion();
//            randomQuizQuestionList.add(quizQuestion);
//        }
//    }
//
//    private interface QuizQuestionChangeListener {
//        void onQuizQuestionChange(JavaQuizQuestion quizQuestion);
//    }
//}