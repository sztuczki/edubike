package pl.fundacjakult.edubike.data_store

import com.google.firebase.database.*
import org.junit.Before
import org.junit.Test
import org.mockito.*
import org.mockito.Mockito.*
import pl.fundacjakult.edubike.data_store.firebase.*
import pl.fundacjakult.edubike.framework.model.JavaContentPart
import pl.fundacjakult.edubike.framework.model.JavaLesson
import pl.fundacjakult.edubike.framework.model.JavaQuizQuestion

class DataStoreTest {

    lateinit var dataStore: DataStore

    @Mock
    lateinit var mockReference: DatabaseReference

    @Mock
    lateinit var mockDataSnapshot: DataSnapshot

    @Mock
    lateinit var onDataAvailableListener: OnDataAvailableListener

    @Mock
    lateinit var referenceStore: ReferenceStore

    @Mock
    lateinit var extractor: FirebaseDataExtractor

    @Captor
    lateinit var captor: ArgumentCaptor<ValueEventListener>

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        dataStore = Mockito.spy(DataStore(referenceStore))
        dataStore.addOnDataAvailableListener(onDataAvailableListener)
    }

    @Test
    fun doAnything() {
        Mockito.`when`(referenceStore.getDatabaseVersionReference()).thenReturn(mockReference)
        Mockito.`when`(mockDataSnapshot.getValue(String::class.java)).thenReturn("123")
        Mockito.`when`(mockReference.addValueEventListener(captor.capture())).thenAnswer { captor.value }
        dataStore.attachVersionListener()
        captor.value.onDataChange(mockDataSnapshot)
        verify(onDataAvailableListener).onDatabaseVersionAvailable("123")
    }

    @Test
    fun `should start listening for lesson list`() {
        val lessonList = ArrayList<JavaLesson>()
        Mockito.`when`(referenceStore.getLessonListReference()).thenReturn(mockReference)
        Mockito.`when`(mockDataSnapshot.getValue(ArrayList::class.java)).thenReturn(lessonList)
        Mockito.`when`(mockReference.addValueEventListener(captor.capture())).thenAnswer { captor.value }
        dataStore.attachLessonListListener()
        captor.value.onDataChange(mockDataSnapshot)
        verify(onDataAvailableListener).onLessonListAvailable(lessonList)

    }

    @Test
    fun `should start listening for content part list`() {
        val contentPartList = ArrayList<JavaContentPart>()
        Mockito.`when`(referenceStore.getContentPartReference()).thenReturn(mockReference)
        Mockito.`when`(extractor.extractContentPartList(mockDataSnapshot)).thenReturn(contentPartList)
        Mockito.`when`(mockReference.addValueEventListener(captor.capture())).thenAnswer { captor.value }
        dataStore.attachContentPartListListener()
        captor.value.onDataChange(mockDataSnapshot)
        verify(onDataAvailableListener).onContentPartListAvailable(contentPartList)
    }

    @Test
    fun shouldStartListeningForQuizQuestionList() {
        val quizQuestionList = ArrayList<JavaQuizQuestion>()
        Mockito.`when`(referenceStore.getQuizQuestionReference()).thenReturn(mockReference)
        Mockito.`when`(extractor.extractQuizQuestionList(mockDataSnapshot)).thenReturn(quizQuestionList)
        Mockito.`when`(mockReference.addValueEventListener(captor.capture())).thenAnswer { captor.value }
        dataStore.attachQuizQuestionListListener()
        captor.value.onDataChange(mockDataSnapshot)
        verify(onDataAvailableListener).onQuizQuestionListAvailable(quizQuestionList)
    }
}